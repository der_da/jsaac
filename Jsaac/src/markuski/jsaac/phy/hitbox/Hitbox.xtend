package markuski.jsaac.phy.hitbox

import aurelienribon.bodyeditor.BodyEditorLoader
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.Fixture
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.utils.Array
import markuski.jsaac.phy.hitbox.world.HitboxWorld
import markuski.jsaac.utils.GameUtils
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite

class Hitbox {
	Body body;
	Array<Fixture> fix;
	HitboxWorld world;
	HitboxDef h;

	@Property
	AnimatedBox2DSprite look;
	
	new (Body b, HitboxDef h, HitboxWorld w){
		this.world=w;
		this.h=h;
		this.body=b;
	}

	/** Get the world body origin position.
	 * @return the world position of the body's origin. */
	public def getPosition() {
		body.position
	}

	/** Get the angle in radians.
	 * @return the current world rotation angle in radians. */
	public def getAngle() {
		body.angle;
	}

	/** Set the linear velocity of the center of mass. */
	public def setLinearVelocity(Vector2 v) {
		body.setLinearVelocity(v);
	}

	/** Get the linear velocity of the center of mass. */
	public def getLinearVelocity() {
		body.linearVelocity;
	}

	/** Set the angular velocity. */
	public def setAngularVelocity(float omega) {
		body.angularVelocity = omega
	}

	/** Get the angular velocity. */
	public def getAngularVelocity() {
		body.angularVelocity
	}

	/** Apply a force at a world point. If the force is not applied at the center of mass, it will generate a torque and affect the
	 * angular velocity. This wakes up the body.
	 * @param force the world force vector, usually in Newtons (N).
	 * @param point the world position of the point of application.
	 * @param wake up the body */
	public def applyForce(Vector2 force, Vector2 point, boolean wake) {
		var force2 = force;
		var point2 = point;
		body.applyForce(force2, point2, wake);
	}

	/** Apply a force to the center of mass. This wakes up the body.
	 * @param force the world force vector, usually in Newtons (N). */
	public def applyForceToCenter(Vector2 force, boolean wake) {
		body.applyForceToCenter(force, wake);
	}

	/** Apply a torque. This affects the angular velocity without affecting the linear velocity of the center of mass. This wakes up
	 * the body.
	 * @param torque about the z-axis (out of the screen), usually in N-m.
	 * @param wake up the body */
	public def applyTorque(float torque, boolean wake) {
		body.applyTorque(torque, wake);
	}

	/** Apply an impulse at a point. This immediately modifies the velocity. It also modifies the angular velocity if the point of
	 * application is not at the center of mass. This wakes up the body.
	 * @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
	 * @param point the world position of the point of application. 
	 * @param wake up the body*/
	public def applyLinearImpulse(Vector2 impulse, Vector2 point, boolean wake) {
		body.applyForce(impulse, point, wake);
	}

	/** Apply an angular impulse.
	 * @param impulse the angular impulse in units of kg*m*m/s */
	public def applyAngularImpulse(float impulse, boolean wake) {
		body.applyAngularImpulse(impulse, wake);
	}

	/** Get the total mass of the body.
	 * @return the mass, usually in kilograms (kg). */
	public def getMass() {
		body.mass;
	}

	/** Get the rotational inertia of the body about the local origin.
	 * @return the rotational inertia, usually in kg-m^2. */
	public def getInertia() {
		body.inertia
	}

	/** Get the world coordinates of a point given the local coordinates.
	 * @param localPoint a point on the body measured relative the the body's origin.
	 * @return the same point expressed in world coordinates. */
	public def getWorldPoint(Vector2 localPoint) {
		body.getWorldPoint(localPoint)
	}

	/** Get the world coordinates of a vector given the local coordinates.
	 * @param localVector a vector fixed in the body.
	 * @return the same vector expressed in world coordinates. */
	public def getWorldVector(Vector2 localVector) {
		body.getWorldVector(localVector);
	}

	/** Gets a local point relative to the body's origin given a world point.
	 * @param worldPoint a point in world coordinates.
	 * @return the corresponding local point relative to the body's origin. */
	public def getLocalPoint(Vector2 worldPoint) {
		body.getLocalPoint(worldPoint);
	}

	/** Gets a local vector given a world vector.
	 * @param worldVector a vector in world coordinates.
	 * @return the corresponding local vector. */
	public def getLocalVector(Vector2 worldVector) {
		body.getLocalVector(worldVector);
	}

	/** Get the world linear velocity of a world point attached to this body.
	 * @param worldPoint a point in world coordinates.
	 * @return the world velocity of a point. */
	public def getLinearVelocityFromWorldPoint(Vector2 worldPoint) {
		body.getLinearVelocityFromWorldPoint(worldPoint);
	}

	/** Get the world velocity of a local point.
	 * @param localPoint a point in local coordinates.
	 * @return the world velocity of a point. */
	public def getLinearVelocityFromLocalPoint(Vector2 localPoint) {
		body.getLinearVelocityFromLocalPoint(localPoint);
	}

	/** Get the linear damping of the body. */
	public def getLinearDamping() {
		body.linearDamping
	}

	/** Set the linear damping of the body. */
	public def setLinearDamping(float linearDamping) {
		body.setLinearDamping(linearDamping);
	}

	/** Get the angular damping of the body. */
	public def getAngularDamping() {
		body.angularDamping
	}

	/** Set the angular damping of the body. */
	public def setAngularDamping(float angularDamping) {
		body.angularDamping = angularDamping;
	}

	/** Set the type of this body. This may alter the mass and velocity. */
	public def setType(BodyDef.BodyType type) {
		body.type = type;
	}

	/** Get the type of this body. */
	public def getType() {
		body.type;
	}

	/** Should this body be treated like a bullet for continuous collision detection? */
	public def setBullet(boolean flag) {
		body.bullet = flag;
	}

	/** Is this body treated like a bullet for continuous collision detection? */
	public def isBullet() {
		body.bullet
	}

	/** You can disable sleeping on this body. If you disable sleeping, the body will never sleep */
	public def setSleepingAllowed(boolean flag) {
		body.sleepingAllowed = flag;
	}

	/** Is this body allowed to sleep */
	public def isSleepingAllowed() {
		body.sleepingAllowed
	}

	/** Set the sleep state of the body. A sleeping body has very low CPU cost.
	 * @param flag set to true to put body to sleep, false to wake it. */
	public def setAwake(boolean flag) {
		body.awake = flag
	}

	/** Get the sleeping state of this body.
	 * @return true if the body is sleeping. */
	public def isAwake() {
		body.awake
	}

	/** Set the active state of the body. An inactive body is not simulated and cannot be collided with or woken up. If you pass a
	 * flag of true, all fixtures will be added to the broad-phase. If you pass a flag of false, all fixtures will be removed from
	 * the broad-phase and all contacts will be destroyed. Fixtures and joints are otherwise unaffected. You may continue to
	 * create/destroy fixtures and joints on inactive bodies. Fixtures on an inactive body are implicitly inactive and will not
	 * participate in collisions, ray-casts, or queries. Joints connected to an inactive body are implicitly inactive. An inactive
	 * body is still owned by a b2World object and remains in the body list. */
	public def setActive(boolean flag) {
		body.active = flag;
	}

	/** Get the active state of the body. */
	public def isActive() {
		body.active
	}

	/** Set this body to have fixed rotation. This causes the mass to be reset. */
	public def setFixedRotation(boolean flag) {
		body.fixedRotation = flag;
	}

	/** Does this body have fixed rotation? */
	public def isFixedRotation() {
		body.fixedRotation;
	}

	/** @return Get the gravity scale of the body. */
	public def getGravityScale() {
		body.gravityScale
	}

	/** Sets the gravity scale of the body */
	public def setGravityScale(float scale) {
		body.gravityScale = scale;
	}

	/** Get the parent world of this body. */
	public def getWorld() {
		world
	}

	/** Get the look */
	public def getLook() {
		body.userData as AnimatedBox2DSprite;
	}

	/** Set the look */
	public def setLook(AnimatedBox2DSprite s) {
		body.userData = s;
	}

	/** Set if this is NonSolid. */
	public def setNonSolid(boolean sensor) {
		fix.forEach[it.sensor = sensor]
	}

	/** Is this fixture a sensor (non-solid)?
	 * @return the true if the shape is a sensor. */
	public def isNonSolid() {
		fix.first.sensor
	}

	/** Test a point for containment in this fixture.
	 * @param p a point in world coordinates. */
	public def testPoint(Vector2 p) {
		fix.forall[it.testPoint(p)]
	}

	/** Set the density of this fixture. */
	public def set_Density(float density) {
		fix.forEach[it.density = density]
		body.resetMassData;
	}

	/** Get the density of this fixture. */
	public def get_Density() {
		fix.first.density
	}

	/** Get the coefficient of friction. */
	public def get_Friction() {
		fix.first.friction;
	}

	/** Set the coefficient of friction. */
	public def set_Friction(float friction) {
		fix.forEach[it.friction = friction]
	}

	/** Get the coefficient of restitution. */
	public def get_Restitution() {
		fix.first.restitution
	}

	/** Set the coefficient of restitution. */
	public def set_Restitution(float restitution) {
		fix.forEach[it.restitution = restitution]
	}
		
}

class HitboxDef {
	BodyDef body;
	FixtureDef fixture;
	@Property
	BodyEditorLoader loader;
	@Property
	String bodyName;
	@Property
	AnimatedBox2DSprite look;
	@Property
	float scale = 1;

	new(BodyEditorLoader loader, String bodyName) {
		body = new BodyDef;
		fixture = new FixtureDef()
		this.loader = loader;
		this.bodyName = bodyName;
		this.look = GameUtils.getAnimatedBox2DSprite(1, 0, loader.getImagePath(bodyName));
	}

	/** The body type: static, kinematic, or dynamic. Note: if a dynamic body would have zero mass, the mass is set to one. **/
	public def setType(BodyDef.BodyType type) {
		body.type = type;
	}

	/** The world position of the body. Avoid creating bodies at the origin since this can lead to many overlapping shapes. **/
	public def setPosition(Vector2 pos) {
		body.position.set(pos);
	}

	/** The world angle of the body in radians. **/
	public def setAngle(float angle) {
		body.angle = angle
	}

	/** The linear velocity of the body's origin in world co-ordinates. **/
	public def setLinearVelocity(Vector2 v) {
		body.linearVelocity.set(v);
	}

	/** The angular velocity of the body. **/
	public def setAngularVelocity(float f) {
		body.angularVelocity = f;
	}

	/** Linear damping is use to reduce the linear velocity. The damping parameter can be larger than 1.0f but the damping effect
	 * becomes sensitive to the time step when the damping parameter is large. **/
	public def setLinearDamping(float f) {
		body.angularDamping = f;
	}

	/** Angular damping is use to reduce the angular velocity. The damping parameter can be larger than 1.0f but the damping effect
	 * becomes sensitive to the time step when the damping parameter is large. **/
	public def setAngularDamping(float f) {
		body.angularDamping = f;
	}

	/** Set this flag to false if this body should never fall asleep. Note that this increases CPU usage. **/
	public def setAlowSleep(boolean b) {
		body.allowSleep = b;
	}

	/** Is this body initially awake or sleeping? **/
	public def setAwake(boolean b) {
		body.awake = b;
	}

	/** The world position of the body. Avoid creating bodies at the origin since this can lead to many overlapping shapes. **/
	public def setFixedRotation(boolean b) {
		body.fixedRotation = b;
	}

	/** Is this a fast moving body that should be prevented from tunneling through other moving bodies? Note that all bodies are
	 * prevented from tunneling through kinematic and static bodies. This setting is only considered on dynamic bodies.
	 * @warning You should use this flag sparingly since it increases processing time. **/
	public def setBullet(boolean b) {
		body.bullet = b;
	}

	/** Does this body start out active? **/
	public def setActive(boolean b) {
		body.active = b;
	}

	/** Scale the gravity applied to this body. **/
	public def setGravityScale(float f) {
		body.gravityScale = f;
	}

	/** The density, usually in kg/m^2. **/
	public def setDensity(float f) {
		fixture.density = f;
	}

	/** The friction coefficient, usually in the range [0,1]. **/
	public def setFriction(float f) {
		fixture.friction = f;
	}

	/** The restitution (elasticity) usually in the range [0,1]. **/
	public def setRestitution(float f) {
		fixture.restitution = f;
	}

	/** A sensor shape collects contact information but never generates a collision response. */
	public def setSensor(boolean b) {
		fixture.isSensor = b;
	}

	/** The body type: static, kinematic, or dynamic. Note: if a dynamic body would have zero mass, the mass is set to one. **/
	public def getType() {
		body.type
	}

	public def getPosition() {
		body.position;
	}

	/** The world angle of the body in radians. **/
	public def getAngle() {
		body.angle
	}

	/** The linear velocity of the body's origin in world co-ordinates. **/
	public def getLinearVelocity() {
		body.linearVelocity;
	}

	/** The angular velocity of the body. **/
	public def getAngularVelocity() {
		body.angularVelocity;
	}

	/** Linear damping is use to reduce the linear velocity. The damping parameter can be larger than 1.0f but the damping effect
	 * becomes sensitive to the time step when the damping parameter is large. **/
	public def getLinearDamping() {
		body.angularDamping;
	}

	/** Angular damping is use to reduce the angular velocity. The damping parameter can be larger than 1.0f but the damping effect
	 * becomes sensitive to the time step when the damping parameter is large. **/
	public def getAngularDamping() {
		body.angularDamping;
	}

	/** Set this flag to false if this body should never fall asleep. Note that this increases CPU usage. **/
	public def getAlowSleep() {
		body.allowSleep;
	}

	/** Is this body initially awake or sleeping? **/
	public def getAwake() {
		body.awake;
	}

	public def getFixedRotation() {
		body.fixedRotation;
	}

	/** Is this a fast moving body that should be prevented from tunneling through other moving bodies? Note that all bodies are
	 * prevented from tunneling through kinematic and static bodies. This setting is only considered on dynamic bodies.
	 * @warning You should use this flag sparingly since it increases processing time. **/
	public def getBullet() {
		body.bullet;
	}

	/** Does this body start out active? **/
	public def getActive() {
		body.active;
	}

	/** Scale the gravity applied to this body. **/
	public def getGravityScale() {
		body.gravityScale;
	}

	/** The density, usually in kg/m^2. **/
	public def getDensity() {
		fixture.density;
	}

	/** The friction coefficient, usually in the range [0,1]. **/
	public def getFriction() {
		fixture.friction;
	}

	/** The restitution (elasticity) usually in the range [0,1]. **/
	public def getRestitution() {
		fixture.restitution;
	}

	/** A sensor shape collects contact information but never generates a collision response. */
	public def getSensor() {
		fixture.isSensor;
	}
	
	@Deprecated
	public def Hitbox toHitbox(HitboxWorld w) {
		w.createHitbox(body, fixture, this);
	}

	public def copy() {
		var h = new HitboxDef(getLoader, getBodyName);
		h.active = getActive;
		h.alowSleep = getAlowSleep;
		h.angle = getAngle;
		h.angularDamping = getAngularDamping;
		h.angularVelocity = getAngularVelocity;
		h.awake = getAwake;
		h.bullet = getBullet;
		h.density = getDensity;
		h.fixedRotation = getFixedRotation;
		h.gravityScale = getGravityScale;
		h.linearDamping = getLinearDamping;
		h.linearVelocity = getLinearVelocity
		h.loader = getLoader;
		h.look = getLook;
		h.position = getPosition;
		h.restitution = getRestitution;
		h.sensor = getSensor;
		h
	}

}
