package markuski.jsaac.phy.hitbox.world

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Disposable
import markuski.jsaac.phy.hitbox.Hitbox
import markuski.jsaac.phy.hitbox.HitboxDef
import markuski.jsaac.utils.TypeStorer2
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite

class HitboxWorld implements Disposable {
	World world;
	Array<Hitbox> hitboxes;
	Array<Body> bodies;
	Box2DDebugRenderer debug;

	new() {
		world = new World(new Vector2, true);
		hitboxes = new Array();
		bodies = new Array();
	}

	public def Hitbox createHitbox(HitboxDef d) {
		d.toHitbox(this) as Hitbox;
	}

	public def destoryHitbox(Hitbox h) {
		var i = hitboxes.indexOf(h, false);
		world.destroyBody(bodies.get(i));
		hitboxes.removeIndex(i);
		hitboxes.shrink
		bodies.shrink;
	}

	@Deprecated
	public def createHitbox(BodyDef b, FixtureDef f, HitboxDef h) {
		var body = world.createBody(b);
		body.userData = h.look
		h.loader.attachFixture(body, h.bodyName, f, h.scale);
		var hitbox = new Hitbox(body, h, this);

		hitboxes.add(hitbox)
		bodies.add(body)
		hitbox;
	}

	public def getContacts() {
		val result = new Array;
		world.contactList.forEach [
			if (it.touching) {
				var hitboxa = hitboxes.get(bodies.indexOf(it.fixtureA.body, false));
				var hitboxb = hitboxes.get(bodies.indexOf(it.fixtureB.body, false));
				result.add(new TypeStorer2(hitboxa, hitboxb));
			}
		]
		result;
	}

	public def step(float timestep) {
		world.step(timestep, 3, 8)
	}

	public def draw(SpriteBatch b) {
		AnimatedBox2DSprite.draw(b, world, true);
	}

	public def drawDebug(SpriteBatch b) {
		if (debug == null) {
			debug = new Box2DDebugRenderer(true, true, true, true, true, true);
		}
		debug.render(world, b.projectionMatrix);
	}

	override dispose() {
		world.dispose;
	}

}
