package markuski.jsaac.entity.health

import com.badlogic.gdx.utils.Array

class Health {
	int currredhp
	int currbluehp
	int currwhitehp;
	int maxredhp;
	boolean canhaveredheart;
	boolean dead = false;
	@Property
	boolean invincible = false;

	Array<(Integer, Boolean, DamageSource)=>boolean> hitcallback;
	Array<()=>void> deadcallback;

	new(int currredhp, int currbluehp, int currwhitehp, int maxredhp, boolean canhaveredheart) {
		this.currredhp = currredhp;
		this.currbluehp = currbluehp;
		this.currwhitehp = currwhitehp;
		this.maxredhp = maxredhp;
		this.canhaveredheart = canhaveredheart;
		this.hitcallback = new Array();
		this.deadcallback = new Array();
	}

	new(int currredhp, int currbluehp, int currwhitehp, int maxredhp) {
		this(currredhp, currbluehp, currwhitehp, maxredhp, true);
	}

	new(int starthp) {
		this(starthp, 0, 0, starthp);
	}

	public def void pickUpHeart(HeartType heart) {
		if (heart == HeartType.redHeart || heart == HeartType.halfredHeart) {
			var redheartdif = maxredhp - currredhp;
			if (redheartdif == 0) {
				return;
			} else if (redheartdif == 1) {
				currredhp = currredhp + 1;
			} else if (redheartdif > 1) {
				if (heart == HeartType.redHeart) {
					currredhp = currredhp + 2;
				} else {
					currredhp = currredhp + 1;
				}
			}
		} else if (heart == HeartType.blueHeart) {
			currbluehp = currbluehp + 1;
		} else if (heart == HeartType.whiteHeart) {
			if (currwhitehp == 1) {
				this.addHeartContainer(false);
			} else {
				currwhitehp = currwhitehp + 1;
			}
		}
	}

	public def void addHeartContainer(boolean filled) {
		if (canhaveredheart) {
			maxredhp = maxredhp + 1;
			if (filled) {
				this.pickUpHeart(HeartType.redHeart);
			}
		} else {
			this.pickUpHeart(HeartType.blueHeart);
		}
	}

	/**
	 * if ignoreblue is true it will damage:
	 * white->red(down to a half heart)->blue
	 */
	private def void damage(int damage, boolean ignoreblue, DamageSource source, boolean force) {
		if (!invincible) {
			if ((this.hitcallback.forall[it.apply(damage, ignoreblue, source)] || force)) {
				var dmg = damage;
				while (dmg >= 1) {
					if (!ignoreblue && currbluehp > 0 || currredhp == 1 && currbluehp > 0) {
						currbluehp = currbluehp - 1;
					} else if (currwhitehp > 0) {
						currwhitehp = currwhitehp - 1;
					} else if (currredhp > 0) {
						currredhp = currredhp - 1;
					}
					if (healthSum <= 0) {
						this.deadcallback.forEach[it.apply]
						dead = true;
					}
					dmg = dmg - 1;
				}
				this.hitcallback.forEach[it.apply(damage, ignoreblue, source);]
			}

		}
	}

	public def void damage(int damage, DamageSource source) {
		this.damage(damage, false, source, false);
	}

	public def void addHitcallback((Integer, Boolean, DamageSource)=>boolean callback) {
		this.hitcallback.add(callback);
	}

	public def void removeHitCallback((Integer, Boolean, DamageSource)=>boolean callback) {
		this.hitcallback.removeValue(callback, false);
	}

	public def void addDeadcallback(()=>void callback) {
		this.deadcallback.add(callback);
	}

	public def void removeDeadCallback(()=>void callback) {
		this.deadcallback.removeValue(callback, false);
	}

	public def boolean isDead() {
		dead;
	}

	public def int getHealthSum() {
		currbluehp + currredhp + currwhitehp
	}

	override toString() {
		super.toString()
		'''currredhp = «currredhp»/«maxredhp», currwhitehp = «currwhitehp», currbluehp = «currbluehp»'''
	}

}

enum HeartType {
	redHeart,
	blueHeart,
	whiteHeart,
	halfredHeart
}
