package markuski.jsaac.entity.manager

import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.Array
import markuski.jsaac.entity.Entity
import markuski.jsaac.entity.EntityDef
import markuski.jsaac.phy.hitbox.world.HitboxWorld
import markuski.jsaac.utils.GameUtils
import markuski.jsaac.utils.TypeStorer2

class EntityManager {

	OrthographicCamera camera;
	SpriteBatch batch;
	HitboxWorld world;
	InputMultiplexer im;

	Array<Entity> entities;

	new(float offsetx, float offsety, int pixeltometer) {
		var i = pixeltometer / 80;
		camera = new OrthographicCamera(16 / i, 9 / i);
		batch = new SpriteBatch()
		world = new HitboxWorld();
		im = new InputMultiplexer;

		camera.position.set = new Vector3(camera.position.x/2, camera.position.y/2, 1)

		camera.update;

		batch.projectionMatrix = camera.combined

		entities = new Array()
	}

	public def void update(float deltaT) {
		entities.forEach[it.updateLifetime(deltaT) it.entityDef.preUpdate(it, GameUtils.newArray(deltaT, batch, camera, im))]
		world.step(deltaT);
		batch.begin
		world.draw(batch);
		batch.end
		entities.forEach[it.entityDef.update(it, GameUtils.newArray(deltaT, batch, camera, im))]

		val ent = new TypeStorer2(null, null)
		world.contacts.forEach[c|
			entities.forEach[
				if (it.hitbox == c.a) {
					ent.a = it;
				} else if (it.hitbox == c.b) {
					ent.b = it;
				}]]
		if (ent.a != null && ent.b != null) {
			ent.a.entityDef.onCollision(ent.a, ent.b);
			ent.b.entityDef.onCollision(ent.b, ent.a);
		}

		entities.forEach[it.entityDef.postUpdate(it, GameUtils.newArray(deltaT, batch, camera, im))]
	}

	public def Entity createEntity(EntityDef ed, float x, float y, Array<String> args) {
		
		var hd = ed.hitboxDef
		hd.position.set(x, y)
		
		var e = new Entity(world.createHitbox(hd), ed.components, ed)
		ed.begin(x, y, args);
		entities.add(e);
		e
	}

	public def Entity createEntity(EntityDef ed, float x, float y) {
		this.createEntity(ed, x, y, new Array());
	}

	public def Entity createEntity(EntityDef ed, float x, float y, Object ... args) {
		val ar = new Array(args.size);
		args.forEach[ar.add(it.toString)];
		this.createEntity(ed, x, y, ar);
	}
	
	public def destroyEntity(Entity e){
		e.dispose
		entities.removeValue(e, false);
	}
}
