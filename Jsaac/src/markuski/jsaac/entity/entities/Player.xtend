package markuski.jsaac.entity.entities

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.utils.Array
import markuski.jsaac.entity.Entity
import markuski.jsaac.entity.EntityDef
import markuski.jsaac.entity.HealthComponent
import markuski.jsaac.entity.health.DamageSource
import markuski.jsaac.entity.health.Health
import markuski.jsaac.phy.hitbox.HitboxDef
import markuski.jsaac.utils.GameUtils

class Player extends EntityDef {
	
	float hitTimeStamp = -1F;
	
	override begin(float x, float y, Array<String> data) {
	}

	override getComponents() {
		GameUtils.newArray(new HealthComponent(new Health(3), [dmg, blueh, ds|this.onHit(dmg, blueh, ds)], [|this.dispose]))
	}

	override protected genHitboxDef() {
		var h = new HitboxDef(COMMON_MODELS, "player");
		h.look = GameUtils.getAnimatedBox2DSprite(0.5F, GameUtils.PLAYER, "img/player.png");
		h.fixedRotation = true;
		h.type = BodyDef.BodyType.DynamicBody;

		h
	}
	
	private def boolean onHit(int dmg, boolean b, DamageSource d){
		hitTimeStamp = (d.to as Entity).lifetime
		HealthComponent.getHealthComponent((d.to as Entity)).obj.invincible = true;
		println("sdasd")
		
		true;
		
	}

	override getUniqueName() {
		"Player"
	}

	override preUpdate(Entity e, Array<?> data) {
		
		var tmppos = new Vector2();
		var speed = 5F;
		if (Gdx.input.isKeyPressed(Input.Keys.W)) {
			tmppos.y = speed;
		} else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
			tmppos.y = - speed;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.A)) {
			tmppos.x = - speed;
		} else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
			tmppos.x = speed;
		}
		e.hitbox.setLinearVelocity(tmppos);
		
		if (HealthComponent.getHealthComponent(e).obj.invincible && hitTimeStamp + 1 <= e.lifetime) {
			HealthComponent.getHealthComponent(e).obj.invincible = false;
		}
	}

	override update(Entity e, Array<?> data) {
	}

	override postUpdate(Entity e, Array<?> data) {
	}

	override onCollision(Entity e, Entity other) {
	}

}
