package markuski.jsaac.entity.entities

import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.utils.Array
import markuski.jsaac.entity.Entity
import markuski.jsaac.entity.EntityComponent
import markuski.jsaac.entity.EntityDef
import markuski.jsaac.entity.health.DamageSource
import markuski.jsaac.entity.health.Health
import markuski.jsaac.phy.hitbox.HitboxDef
import markuski.jsaac.utils.GameUtils

class Spike extends EntityDef {
	
	override begin(float x, float y, Array<String> data) {
	}
	
	override getComponents() {
	}
	
	override protected genHitboxDef() {
		var h  = new HitboxDef(COMMON_MODELS, "spike");
		h.type = BodyDef.BodyType.StaticBody;
		h.sensor = true;
		h.look = GameUtils.getAnimatedBox2DSprite(1, GameUtils.BLOCK, "img/spike.png");
		
		h
	}
	
	override getUniqueName() {
		"spike";
	}
	
	override preUpdate(Entity e, Array<?> data) {
	}
	
	override update(Entity e, Array<?> data) {
	}
	
	override postUpdate(Entity e, Array<?> data) {
	}
	
	override onCollision(Entity e, Entity other) {
		var comp = other.getComponent(EntityComponent.HEALTH)
		if(comp!= null){
			(comp.obj as Health).damage(1, DamageSource.entity(e, other))
		}
	}

	
}