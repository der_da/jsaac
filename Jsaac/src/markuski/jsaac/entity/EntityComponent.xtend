package markuski.jsaac.entity

import markuski.jsaac.entity.health.Health
import markuski.jsaac.entity.ai.Ai
import markuski.jsaac.entity.health.DamageSource

@Data
class EntityComponent<T> {
	
	public static final String HEALTH = "health"
	
	String name;
	T obj;
}

class HealthComponent extends EntityComponent<Health>{
	
	public static def getHealthComponent(Entity e){
		e.getComponent(HealthComponent.HEALTH)as HealthComponent
	}
	
	new(Health obj, (Integer, Boolean, DamageSource)=>boolean hitcallback, ()=>void deadcallback) {
		super(HEALTH, obj)
		obj.addHitcallback(hitcallback)
		obj.addDeadcallback(deadcallback)
	}
	
}

class AiComponent extends EntityComponent<Ai>{
	new(Ai obj) {
		super("ai", obj)
	}
	
}