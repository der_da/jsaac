package markuski.jsaac.entity

import aurelienribon.bodyeditor.BodyEditorLoader
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.JsonValue
import markuski.jsaac.access.GameAccess
import markuski.jsaac.entity.manager.EntityManager
import markuski.jsaac.phy.hitbox.Hitbox
import markuski.jsaac.phy.hitbox.HitboxDef
import markuski.jsaac.utils.TypeStorer

abstract class EntityDef implements Disposable {
	public static val COMMON_MODELS = new BodyEditorLoader(Gdx.files.internal("model/common.json"));

	/**
	 * Called on spawn
	 * @param x The x position
	 * @param y The y position
	 * @param data The Data(for deserialization)
	 */
	public def void begin(float x, float y, Array<String> data)

	/**
	 * The components that the Entity will have
	 */
	public def Array<EntityComponent<?>> getComponents();

	/**
	 * The hitboxDef for the Hitbox of the Entity
	 */
	protected def HitboxDef genHitboxDef();

	/**
	 * A unique Name
	 */
	public def String getUniqueName();

	/**
	 * Update
	 */
	public def void preUpdate(Entity e, Array<?> data);

	/**
	 * Update
	 */
	public def void update(Entity e, Array<?> data);

	/**
	 * Update
	 */
	public def void postUpdate(Entity e, Array<?> data);

	/**
	 * Collision with entity
	 */
	public def void onCollision(Entity e, Entity other);

	public def getHitboxDef() {
		this.genHitboxDef;
	}

	/**
	 * Simple toString()
	 */
	override toString() {
		super.toString
		'''Entity_«this.uniqueName»@«Integer.toHexString(hashCode())»'''
	}

	/**
	 * The Data (for serialization)
	 */
	public def getData(Entity e) {
		new Array<String>
	}

	override dispose() {
	}

}

class Entity implements Disposable {
	@Property Hitbox hitbox;
	@Property EntityDef EntityDef;
	float lifetime = 0F;
	Array<? extends EntityComponent<?>> components

	new(Hitbox h, Array<EntityComponent<?>> components, EntityDef d) {
		this.hitbox = h;
		this.components = components;
		this.entityDef = d;
	}

	public def EntityComponent<?> getComponent(String name) {
		val TypeStorer<EntityComponent<?>> result = new TypeStorer(null);
		components.forEach[
			if (it.name == name) {
				result.a = it;
			}]
		result.a
	}

	public def getLifetime() {
		lifetime
	}

	public def updateLifetime(float deltaT) {
		lifetime = lifetime + deltaT
	}

	override dispose() {
		hitbox.world.destoryHitbox(hitbox)
	}

}

class EntitySerializer {
	public static def serilize(Entity obj) {
		var pos = obj.hitbox.position;
		var str = obj.entityDef.class.name
		var str2 = Entity.package.name + ".entities."
		var clazz = if(str.startsWith(str2)) str.replace(str2, "") else str
		'''["«clazz»", «GameAccess.JSON.toJson(obj.entityDef.getData(obj))», «pos.x», «pos.y»]
			'''
	}

	public static def deserilize(JsonValue jsonData) {
		var str = jsonData.get(0).asString
		try {
			Class.forName(str).getConstructor().newInstance() as EntityDef
		} catch (Exception e) {
			try {
				Class.forName(Entity.package.name + ".entities." + str).getConstructor().newInstance() as EntityDef
			} catch (Exception e1) {
				throw e1;
			}
		}
	}

	public static def deserilize(JsonValue jsonData, EntityManager ew) {
		println(GameAccess.JSON.fromJson(Array, jsonData.get(1).toString))
		ew.createEntity(deserilize(jsonData), jsonData.get(2).asFloat, jsonData.get(3).asFloat,
			GameAccess.JSON.fromJson(Array, jsonData.get(1).toString))
	}

}
