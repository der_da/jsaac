package markuski.jsaac.map

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonValue

class Map {
	new(FileHandle mapHome) {
	}

}

package class MapGenerator {
	
	val JsonValue floorInfo;
	val Array<JsonValue> rooms;
	
	new(FileHandle mapHome) {
		var json = new JsonReader();
		floorInfo = json.parse(mapHome.child("map.json"));
		rooms = new Array();
		
	}
	
}
