package markuski.jsaac.map

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.JsonReader
import markuski.jsaac.entity.manager.EntityManager
import markuski.jsaac.entity.EntitySerializer

class Room {
}

class RoomGenerator {

	int type;
	int height;
	int width;

	new(FileHandle roomFile, EntityManager w, int x, int y) {
		if (roomFile.extension.toLowerCase != "jroom") {
			throw new RuntimeException("not a correct Jsaac Room");
		}

		var room = new JsonReader().parse(roomFile);
		this.type = room.get("type").asInt;
		this.height = room.get("size").get("h").asInt;
		this.width = room.get("size").get("w").asInt;
		room.get("entities").forEach[println(it + "   " + w)EntitySerializer.deserilize(it, w)]
	}
	
	
}
