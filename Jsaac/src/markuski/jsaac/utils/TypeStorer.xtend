package markuski.jsaac.utils

class TypeStorer<TypeA> {
	@Property TypeA a;

	new(TypeA a) {
		this.a = a;
	}
	
	public def getSize(){
		1;
	}
}

class TypeStorer2<TypeA, TypeB> extends TypeStorer<TypeA>{
	@Property TypeB b;

	new(TypeA a, TypeB b) {
		super(a)
		this.b = b;
	}
	public override getSize(){
		2;
	}

}
class TypeStorer3<TypeA, TypeB, TypeC> extends TypeStorer2<TypeA, TypeB>{
	@Property TypeC c;

	new(TypeA a, TypeB b, TypeC c) {
		super(a, b)
		this.c = c;
	}
	public override getSize(){
		3;
	}

}

//
class TypeStorer5<TypeA, TypeB, TypeC, TypeD, TypeE, TypeF> {
	@Property TypeA a;
	@Property TypeB b;
	@Property TypeC c;
	@Property TypeD d;
	@Property TypeE e;
	@Property TypeF f;

	new(TypeA a, TypeB b, TypeC c, TypeD d, TypeE e, TypeF f) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
	}

}
