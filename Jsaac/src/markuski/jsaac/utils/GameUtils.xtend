package markuski.jsaac.utils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.GdxRuntimeException
import java.util.Map
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite
import net.dermetfan.utils.libgdx.graphics.AnimatedSprite

class GameUtils {
	
	public static val BLOCK = 1;
	public static val PLAYER = 100;
	
	
	private static Map<String, Sprite> Spritecache = newHashMap();

	public static def nameAndFPS() {
		Gdx.graphics.setTitle = '''JSaac FPS:«Gdx.graphics.framesPerSecond»''';
	}

	public static def getSprite(String path, int w, int h) {
		if (Spritecache.containsKey(path + "w" + w + "h" + h)) {
			return Spritecache.get(path + "w" + w + "h" + h);
		} else {
			var s = try {
				new Sprite(new Texture(path));
			} catch (GdxRuntimeException e) {
				new Sprite(new Texture("img/imgerror.png"));
			}
			s.setSize(w, h);
			Spritecache.put(path + "w" + w + "h" + h, s);
			s;
		}
	}

	public static def getAnimatedBox2DSprite(float frameDuration, int zindex, String... paths) {
		var keyFrames = new Array();
		var i = 0
		while (i < paths.length) {
			keyFrames.add(GameUtils.getSprite(paths.get(i), 80, 80))
			i = i + 1
		}

		var sprite = new AnimatedBox2DSprite(new AnimatedSprite(new Animation(frameDuration, keyFrames)))
		sprite.z=zindex;
		sprite;
	}
	
	public static def <T> Array<T> newArray(T ... initail){
		var result = new Array
		result.addAll(initail)
		result
	}
}
