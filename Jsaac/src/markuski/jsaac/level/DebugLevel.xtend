package markuski.jsaac.level

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL11
import markuski.jsaac.entity.entities.Player
import markuski.jsaac.entity.manager.EntityManager
import markuski.jsaac.map.RoomGenerator

class DebugLevel extends InputAdapter implements Screen {
	
	EntityManager em;
	override dispose() {
		this.hide;
	}

	override hide() {
	}

	override pause() {
	}

	override render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT);
		em.update(delta)
		//ew.updateAndDraw(delta);
		//ew.world.drawDebug(ew.batch)
	}

	override resize(int width, int height) {
		//ew.resize(width, height);
	}

	override resume() {
	}

	override show() {
		//ew = new EntityWorld(Gdx.graphics.width, Gdx.graphics.height, 80, new Player(), new Array);
		//ew.setOffsetX(1);
		//ew.setOffsetY(2);
		em = new EntityManager(1, 2, 80)
		em.createEntity(new Player, 1, 0);
		new RoomGenerator(Gdx.files.internal("room/testRoom.jroom"), em, 0, 0);
	}
}