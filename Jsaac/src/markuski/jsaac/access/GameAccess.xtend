package markuski.jsaac.access

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json

class GameAccess {
	public static final Game GAME = Gdx.app.applicationListener as Game;
	public static final Json JSON = new Json();

}
