package markuski.jsaac

import com.badlogic.gdx.Game
import markuski.jsaac.access.GameAccess
import markuski.jsaac.level.DebugLevel

class Jsaac extends Game {
	
	override create() {		
		GameAccess.GAME.screen = new DebugLevel();
	}
	
}