package markuski.jsaac.entity;

import markuski.jsaac.entity.EntityComponent;
import markuski.jsaac.entity.ai.Ai;

@SuppressWarnings("all")
public class AiComponent extends EntityComponent<Ai> {
  public AiComponent(final Ai obj) {
    super("ai", obj);
  }
}
