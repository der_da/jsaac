package markuski.jsaac.entity;

import aurelienribon.bodyeditor.BodyEditorLoader;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import markuski.jsaac.entity.Entity;
import markuski.jsaac.entity.EntityComponent;
import markuski.jsaac.phy.hitbox.HitboxDef;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function0;

@SuppressWarnings("all")
public abstract class EntityDef implements Disposable {
  public final static BodyEditorLoader COMMON_MODELS = new Function0<BodyEditorLoader>() {
    public BodyEditorLoader apply() {
      FileHandle _internal = Gdx.files.internal("model/common.json");
      BodyEditorLoader _bodyEditorLoader = new BodyEditorLoader(_internal);
      return _bodyEditorLoader;
    }
  }.apply();
  
  /**
   * Called on spawn
   * @param x The x position
   * @param y The y position
   * @param data The Data(for deserialization)
   */
  public abstract void begin(final float x, final float y, final Array<String> data);
  
  /**
   * The components that the Entity will have
   */
  public abstract Array<EntityComponent<? extends Object>> getComponents();
  
  /**
   * The hitboxDef for the Hitbox of the Entity
   */
  protected abstract HitboxDef genHitboxDef();
  
  /**
   * A unique Name
   */
  public abstract String getUniqueName();
  
  /**
   * Update
   */
  public abstract void preUpdate(final Entity e, final Array<? extends Object> data);
  
  /**
   * Update
   */
  public abstract void update(final Entity e, final Array<? extends Object> data);
  
  /**
   * Update
   */
  public abstract void postUpdate(final Entity e, final Array<? extends Object> data);
  
  /**
   * Collision with entity
   */
  public abstract void onCollision(final Entity e, final Entity other);
  
  public HitboxDef getHitboxDef() {
    HitboxDef _genHitboxDef = this.genHitboxDef();
    return _genHitboxDef;
  }
  
  /**
   * Simple toString()
   */
  public String toString() {
    String _xblockexpression = null;
    {
      super.toString();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Entity_");
      String _uniqueName = this.getUniqueName();
      _builder.append(_uniqueName, "");
      _builder.append("@");
      int _hashCode = this.hashCode();
      String _hexString = Integer.toHexString(_hashCode);
      _builder.append(_hexString, "");
      _xblockexpression = (_builder.toString());
    }
    return _xblockexpression;
  }
  
  /**
   * The Data (for serialization)
   */
  public Array<String> getData(final Entity e) {
    Array<String> _array = new Array<String>();
    return _array;
  }
  
  public void dispose() {
  }
}
