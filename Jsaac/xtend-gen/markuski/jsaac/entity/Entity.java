package markuski.jsaac.entity;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.google.common.base.Objects;
import markuski.jsaac.entity.EntityComponent;
import markuski.jsaac.entity.EntityDef;
import markuski.jsaac.phy.hitbox.Hitbox;
import markuski.jsaac.phy.hitbox.world.HitboxWorld;
import markuski.jsaac.utils.TypeStorer;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class Entity implements Disposable {
  private Hitbox _hitbox;
  
  public Hitbox getHitbox() {
    return this._hitbox;
  }
  
  public void setHitbox(final Hitbox hitbox) {
    this._hitbox = hitbox;
  }
  
  private EntityDef _EntityDef;
  
  public EntityDef getEntityDef() {
    return this._EntityDef;
  }
  
  public void setEntityDef(final EntityDef EntityDef) {
    this._EntityDef = EntityDef;
  }
  
  private float lifetime = 0F;
  
  private Array<? extends EntityComponent<? extends Object>> components;
  
  public Entity(final Hitbox h, final Array<EntityComponent<? extends Object>> components, final EntityDef d) {
    this.setHitbox(h);
    this.components = components;
    this.setEntityDef(d);
  }
  
  public EntityComponent<? extends Object> getComponent(final String name) {
    EntityComponent<? extends Object> _xblockexpression = null;
    {
      TypeStorer<EntityComponent<? extends Object>> _typeStorer = new TypeStorer<EntityComponent<?>>(null);
      final TypeStorer<EntityComponent<?>> result = _typeStorer;
      final Procedure1<EntityComponent<? extends Object>> _function = new Procedure1<EntityComponent<? extends Object>>() {
        public void apply(final EntityComponent<? extends Object> it) {
          String _name = it.getName();
          boolean _equals = Objects.equal(_name, name);
          if (_equals) {
            result.setA(it);
          }
        }
      };
      IterableExtensions.forEach(this.components, _function);
      EntityComponent<? extends Object> _a = result.getA();
      _xblockexpression = (_a);
    }
    return _xblockexpression;
  }
  
  public float getLifetime() {
    return this.lifetime;
  }
  
  public float updateLifetime(final float deltaT) {
    float _lifetime = this.lifetime = (this.lifetime + deltaT);
    return _lifetime;
  }
  
  public void dispose() {
    Hitbox _hitbox = this.getHitbox();
    HitboxWorld _world = _hitbox.getWorld();
    Hitbox _hitbox_1 = this.getHitbox();
    _world.destoryHitbox(_hitbox_1);
  }
}
