package markuski.jsaac.entity;

import markuski.jsaac.entity.Entity;
import markuski.jsaac.entity.EntityComponent;
import markuski.jsaac.entity.health.DamageSource;
import markuski.jsaac.entity.health.Health;
import org.eclipse.xtext.xbase.lib.Functions.Function3;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;

@SuppressWarnings("all")
public class HealthComponent extends EntityComponent<Health> {
  public static HealthComponent getHealthComponent(final Entity e) {
    EntityComponent<? extends Object> _component = e.getComponent(HealthComponent.HEALTH);
    return ((HealthComponent) _component);
  }
  
  public HealthComponent(final Health obj, final Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean> hitcallback, final Procedure0 deadcallback) {
    super(EntityComponent.HEALTH, obj);
    obj.addHitcallback(hitcallback);
    obj.addDeadcallback(deadcallback);
  }
}
