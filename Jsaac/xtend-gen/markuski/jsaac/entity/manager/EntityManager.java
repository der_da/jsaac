package markuski.jsaac.entity.manager;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.google.common.base.Objects;
import java.util.List;
import markuski.jsaac.entity.Entity;
import markuski.jsaac.entity.EntityComponent;
import markuski.jsaac.entity.EntityDef;
import markuski.jsaac.phy.hitbox.Hitbox;
import markuski.jsaac.phy.hitbox.HitboxDef;
import markuski.jsaac.phy.hitbox.world.HitboxWorld;
import markuski.jsaac.utils.GameUtils;
import markuski.jsaac.utils.TypeStorer2;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class EntityManager {
  private OrthographicCamera camera;
  
  private SpriteBatch batch;
  
  private HitboxWorld world;
  
  private InputMultiplexer im;
  
  private Array<Entity> entities;
  
  public EntityManager(final float offsetx, final float offsety, final int pixeltometer) {
    int i = (pixeltometer / 80);
    OrthographicCamera _orthographicCamera = new OrthographicCamera((16 / i), (9 / i));
    this.camera = _orthographicCamera;
    SpriteBatch _spriteBatch = new SpriteBatch();
    this.batch = _spriteBatch;
    HitboxWorld _hitboxWorld = new HitboxWorld();
    this.world = _hitboxWorld;
    InputMultiplexer _inputMultiplexer = new InputMultiplexer();
    this.im = _inputMultiplexer;
    Vector3 _vector3 = new Vector3((this.camera.position.x / 2), (this.camera.position.y / 2), 1);
    this.camera.position.set(_vector3);
    this.camera.update();
    this.batch.setProjectionMatrix(this.camera.combined);
    Array<Entity> _array = new Array<Entity>();
    this.entities = _array;
  }
  
  public void update(final float deltaT) {
    final Procedure1<Entity> _function = new Procedure1<Entity>() {
      public void apply(final Entity it) {
        it.updateLifetime(deltaT);
        EntityDef _entityDef = it.getEntityDef();
        Array<Object> _newArray = GameUtils.<Object>newArray(Float.valueOf(deltaT), EntityManager.this.batch, EntityManager.this.camera, EntityManager.this.im);
        _entityDef.preUpdate(it, _newArray);
      }
    };
    IterableExtensions.<Entity>forEach(this.entities, _function);
    this.world.step(deltaT);
    this.batch.begin();
    this.world.draw(this.batch);
    this.batch.end();
    final Procedure1<Entity> _function_1 = new Procedure1<Entity>() {
      public void apply(final Entity it) {
        EntityDef _entityDef = it.getEntityDef();
        Array<Object> _newArray = GameUtils.<Object>newArray(Float.valueOf(deltaT), EntityManager.this.batch, EntityManager.this.camera, EntityManager.this.im);
        _entityDef.update(it, _newArray);
      }
    };
    IterableExtensions.<Entity>forEach(this.entities, _function_1);
    TypeStorer2<Entity,Entity> _typeStorer2 = new TypeStorer2<Entity, Entity>(null, null);
    final TypeStorer2<Entity,Entity> ent = _typeStorer2;
    Array<TypeStorer2<Hitbox,Hitbox>> _contacts = this.world.getContacts();
    final Procedure1<TypeStorer2<Hitbox,Hitbox>> _function_2 = new Procedure1<TypeStorer2<Hitbox,Hitbox>>() {
      public void apply(final TypeStorer2<Hitbox,Hitbox> c) {
        final Procedure1<Entity> _function = new Procedure1<Entity>() {
          public void apply(final Entity it) {
            Hitbox _hitbox = it.getHitbox();
            Hitbox _a = c.getA();
            boolean _equals = Objects.equal(_hitbox, _a);
            if (_equals) {
              ent.setA(it);
            } else {
              Hitbox _hitbox_1 = it.getHitbox();
              Hitbox _b = c.getB();
              boolean _equals_1 = Objects.equal(_hitbox_1, _b);
              if (_equals_1) {
                ent.setB(it);
              }
            }
          }
        };
        IterableExtensions.<Entity>forEach(EntityManager.this.entities, _function);
      }
    };
    IterableExtensions.<TypeStorer2<Hitbox,Hitbox>>forEach(_contacts, _function_2);
    boolean _and = false;
    Entity _a = ent.getA();
    boolean _notEquals = (!Objects.equal(_a, null));
    if (!_notEquals) {
      _and = false;
    } else {
      Entity _b = ent.getB();
      boolean _notEquals_1 = (!Objects.equal(_b, null));
      _and = (_notEquals && _notEquals_1);
    }
    if (_and) {
      Entity _a_1 = ent.getA();
      EntityDef _entityDef = _a_1.getEntityDef();
      Entity _a_2 = ent.getA();
      Entity _b_1 = ent.getB();
      _entityDef.onCollision(_a_2, _b_1);
      Entity _b_2 = ent.getB();
      EntityDef _entityDef_1 = _b_2.getEntityDef();
      Entity _b_3 = ent.getB();
      Entity _a_3 = ent.getA();
      _entityDef_1.onCollision(_b_3, _a_3);
    }
    final Procedure1<Entity> _function_3 = new Procedure1<Entity>() {
      public void apply(final Entity it) {
        EntityDef _entityDef = it.getEntityDef();
        Array<Object> _newArray = GameUtils.<Object>newArray(Float.valueOf(deltaT), EntityManager.this.batch, EntityManager.this.camera, EntityManager.this.im);
        _entityDef.postUpdate(it, _newArray);
      }
    };
    IterableExtensions.<Entity>forEach(this.entities, _function_3);
  }
  
  public Entity createEntity(final EntityDef ed, final float x, final float y, final Array<String> args) {
    Entity _xblockexpression = null;
    {
      HitboxDef hd = ed.getHitboxDef();
      Vector2 _position = hd.getPosition();
      _position.set(x, y);
      Hitbox _createHitbox = this.world.createHitbox(hd);
      Array<EntityComponent<? extends Object>> _components = ed.getComponents();
      Entity _entity = new Entity(_createHitbox, _components, ed);
      Entity e = _entity;
      ed.begin(x, y, args);
      this.entities.add(e);
      _xblockexpression = (e);
    }
    return _xblockexpression;
  }
  
  public Entity createEntity(final EntityDef ed, final float x, final float y) {
    Array<String> _array = new Array<String>();
    Entity _createEntity = this.createEntity(ed, x, y, _array);
    return _createEntity;
  }
  
  public Entity createEntity(final EntityDef ed, final float x, final float y, final Object... args) {
    Entity _xblockexpression = null;
    {
      int _size = ((List<Object>)Conversions.doWrapArray(args)).size();
      Array<String> _array = new Array<String>(_size);
      final Array<String> ar = _array;
      final Procedure1<Object> _function = new Procedure1<Object>() {
        public void apply(final Object it) {
          String _string = it.toString();
          ar.add(_string);
        }
      };
      IterableExtensions.<Object>forEach(((Iterable<Object>)Conversions.doWrapArray(args)), _function);
      Entity _createEntity = this.createEntity(ed, x, y, ar);
      _xblockexpression = (_createEntity);
    }
    return _xblockexpression;
  }
  
  public boolean destroyEntity(final Entity e) {
    boolean _xblockexpression = false;
    {
      e.dispose();
      boolean _removeValue = this.entities.removeValue(e, false);
      _xblockexpression = (_removeValue);
    }
    return _xblockexpression;
  }
}
