package markuski.jsaac.entity.entities;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.Array;
import com.google.common.base.Objects;
import markuski.jsaac.entity.Entity;
import markuski.jsaac.entity.EntityComponent;
import markuski.jsaac.entity.EntityDef;
import markuski.jsaac.entity.health.DamageSource;
import markuski.jsaac.entity.health.Health;
import markuski.jsaac.phy.hitbox.HitboxDef;
import markuski.jsaac.utils.GameUtils;
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite;

@SuppressWarnings("all")
public class Spike extends EntityDef {
  public void begin(final float x, final float y, final Array<String> data) {
  }
  
  public Array<EntityComponent<? extends Object>> getComponents() {
    return null;
  }
  
  protected HitboxDef genHitboxDef() {
    HitboxDef _xblockexpression = null;
    {
      HitboxDef _hitboxDef = new HitboxDef(EntityDef.COMMON_MODELS, "spike");
      HitboxDef h = _hitboxDef;
      h.setType(BodyDef.BodyType.StaticBody);
      h.setSensor(true);
      AnimatedBox2DSprite _animatedBox2DSprite = GameUtils.getAnimatedBox2DSprite(1, GameUtils.BLOCK, "img/spike.png");
      h.setLook(_animatedBox2DSprite);
      _xblockexpression = (h);
    }
    return _xblockexpression;
  }
  
  public String getUniqueName() {
    return "spike";
  }
  
  public void preUpdate(final Entity e, final Array<? extends Object> data) {
  }
  
  public void update(final Entity e, final Array<? extends Object> data) {
  }
  
  public void postUpdate(final Entity e, final Array<? extends Object> data) {
  }
  
  public void onCollision(final Entity e, final Entity other) {
    EntityComponent<? extends Object> comp = other.getComponent(EntityComponent.HEALTH);
    boolean _notEquals = (!Objects.equal(comp, null));
    if (_notEquals) {
      Object _obj = comp.getObj();
      DamageSource _entity = DamageSource.entity(e, other);
      ((Health) _obj).damage(1, _entity);
    }
  }
}
