package markuski.jsaac.entity.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.Array;
import markuski.jsaac.entity.Entity;
import markuski.jsaac.entity.EntityComponent;
import markuski.jsaac.entity.EntityDef;
import markuski.jsaac.entity.HealthComponent;
import markuski.jsaac.entity.health.DamageSource;
import markuski.jsaac.entity.health.Health;
import markuski.jsaac.phy.hitbox.Hitbox;
import markuski.jsaac.phy.hitbox.HitboxDef;
import markuski.jsaac.utils.GameUtils;
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite;
import org.eclipse.xtext.xbase.lib.Functions.Function3;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;

@SuppressWarnings("all")
public class Player extends EntityDef {
  private float hitTimeStamp = (-1F);
  
  public void begin(final float x, final float y, final Array<String> data) {
  }
  
  public Array<EntityComponent<? extends Object>> getComponents() {
    Health _health = new Health(3);
    final Function3<Integer,Boolean,DamageSource,Boolean> _function = new Function3<Integer,Boolean,DamageSource,Boolean>() {
      public Boolean apply(final Integer dmg, final Boolean blueh, final DamageSource ds) {
        boolean _onHit = Player.this.onHit((dmg).intValue(), (blueh).booleanValue(), ds);
        return Boolean.valueOf(_onHit);
      }
    };
    final Procedure0 _function_1 = new Procedure0() {
      public void apply() {
        Player.this.dispose();
      }
    };
    HealthComponent _healthComponent = new HealthComponent(_health, _function, _function_1);
    Array<EntityComponent<? extends Object>> _newArray = GameUtils.<EntityComponent<? extends Object>>newArray(_healthComponent);
    return _newArray;
  }
  
  protected HitboxDef genHitboxDef() {
    HitboxDef _xblockexpression = null;
    {
      HitboxDef _hitboxDef = new HitboxDef(EntityDef.COMMON_MODELS, "player");
      HitboxDef h = _hitboxDef;
      AnimatedBox2DSprite _animatedBox2DSprite = GameUtils.getAnimatedBox2DSprite(0.5F, GameUtils.PLAYER, "img/player.png");
      h.setLook(_animatedBox2DSprite);
      h.setFixedRotation(true);
      h.setType(BodyDef.BodyType.DynamicBody);
      _xblockexpression = (h);
    }
    return _xblockexpression;
  }
  
  private boolean onHit(final int dmg, final boolean b, final DamageSource d) {
    boolean _xblockexpression = false;
    {
      Object _to = d.getTo();
      float _lifetime = ((Entity) _to).getLifetime();
      this.hitTimeStamp = _lifetime;
      Object _to_1 = d.getTo();
      HealthComponent _healthComponent = HealthComponent.getHealthComponent(((Entity) _to_1));
      Health _obj = _healthComponent.getObj();
      _obj.setInvincible(true);
      InputOutput.<String>println("sdasd");
      _xblockexpression = (true);
    }
    return _xblockexpression;
  }
  
  public String getUniqueName() {
    return "Player";
  }
  
  public void preUpdate(final Entity e, final Array<? extends Object> data) {
    Vector2 _vector2 = new Vector2();
    Vector2 tmppos = _vector2;
    float speed = 5F;
    boolean _isKeyPressed = Gdx.input.isKeyPressed(Input.Keys.W);
    if (_isKeyPressed) {
      tmppos.y = speed;
    } else {
      boolean _isKeyPressed_1 = Gdx.input.isKeyPressed(Input.Keys.S);
      if (_isKeyPressed_1) {
        tmppos.y = (-speed);
      }
    }
    boolean _isKeyPressed_2 = Gdx.input.isKeyPressed(Input.Keys.A);
    if (_isKeyPressed_2) {
      tmppos.x = (-speed);
    } else {
      boolean _isKeyPressed_3 = Gdx.input.isKeyPressed(Input.Keys.D);
      if (_isKeyPressed_3) {
        tmppos.x = speed;
      }
    }
    Hitbox _hitbox = e.getHitbox();
    _hitbox.setLinearVelocity(tmppos);
    boolean _and = false;
    HealthComponent _healthComponent = HealthComponent.getHealthComponent(e);
    Health _obj = _healthComponent.getObj();
    boolean _isInvincible = _obj.isInvincible();
    if (!_isInvincible) {
      _and = false;
    } else {
      float _lifetime = e.getLifetime();
      boolean _lessEqualsThan = ((this.hitTimeStamp + 1) <= _lifetime);
      _and = (_isInvincible && _lessEqualsThan);
    }
    if (_and) {
      HealthComponent _healthComponent_1 = HealthComponent.getHealthComponent(e);
      Health _obj_1 = _healthComponent_1.getObj();
      _obj_1.setInvincible(false);
    }
  }
  
  public void update(final Entity e, final Array<? extends Object> data) {
  }
  
  public void postUpdate(final Entity e, final Array<? extends Object> data) {
  }
  
  public void onCollision(final Entity e, final Entity other) {
  }
}
