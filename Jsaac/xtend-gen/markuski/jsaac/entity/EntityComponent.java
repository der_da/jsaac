package markuski.jsaac.entity;

import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class EntityComponent<T extends Object> {
  public static String HEALTH = "health";
  
  private final String _name;
  
  public String getName() {
    return this._name;
  }
  
  private final T _obj;
  
  public T getObj() {
    return this._obj;
  }
  
  public EntityComponent(final String name, final T obj) {
    super();
    this._name = name;
    this._obj = obj;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((_name== null) ? 0 : _name.hashCode());
    result = prime * result + ((_obj== null) ? 0 : _obj.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    EntityComponent other = (EntityComponent) obj;
    if (_name == null) {
      if (other._name != null)
        return false;
    } else if (!_name.equals(other._name))
      return false;
    if (_obj == null) {
      if (other._obj != null)
        return false;
    } else if (!_obj.equals(other._obj))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
