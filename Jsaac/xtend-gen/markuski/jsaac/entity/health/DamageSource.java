package markuski.jsaac.entity.health;

import markuski.jsaac.entity.Entity;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class DamageSource {
  public static DamageSource entity(final Object e, final Object e2) {
    DamageSource _damageSource = new DamageSource(e, e2);
    return _damageSource;
  }
  
  private final Object _from;
  
  public Object getFrom() {
    return this._from;
  }
  
  private final Object _to;
  
  public Object getTo() {
    return this._to;
  }
  
  public boolean wasEntitiy() {
    boolean _xifexpression = false;
    Object _from = this.getFrom();
    if ((_from instanceof Entity)) {
      _xifexpression = true;
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  public DamageSource(final Object from, final Object to) {
    super();
    this._from = from;
    this._to = to;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((_from== null) ? 0 : _from.hashCode());
    result = prime * result + ((_to== null) ? 0 : _to.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DamageSource other = (DamageSource) obj;
    if (_from == null) {
      if (other._from != null)
        return false;
    } else if (!_from.equals(other._from))
      return false;
    if (_to == null) {
      if (other._to != null)
        return false;
    } else if (!_to.equals(other._to))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
