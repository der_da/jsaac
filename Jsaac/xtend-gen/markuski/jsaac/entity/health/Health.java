package markuski.jsaac.entity.health;

import com.badlogic.gdx.utils.Array;
import com.google.common.base.Objects;
import markuski.jsaac.entity.health.DamageSource;
import markuski.jsaac.entity.health.HeartType;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function3;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure0;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class Health {
  private int currredhp;
  
  private int currbluehp;
  
  private int currwhitehp;
  
  private int maxredhp;
  
  private boolean canhaveredheart;
  
  private boolean dead = false;
  
  private boolean _invincible = false;
  
  public boolean isInvincible() {
    return this._invincible;
  }
  
  public void setInvincible(final boolean invincible) {
    this._invincible = invincible;
  }
  
  private Array<Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean>> hitcallback;
  
  private Array<Procedure0> deadcallback;
  
  public Health(final int currredhp, final int currbluehp, final int currwhitehp, final int maxredhp, final boolean canhaveredheart) {
    this.currredhp = currredhp;
    this.currbluehp = currbluehp;
    this.currwhitehp = currwhitehp;
    this.maxredhp = maxredhp;
    this.canhaveredheart = canhaveredheart;
    Array<Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean>> _array = new Array<Function3<? super Integer, ? super Boolean, ? super DamageSource, ? extends Boolean>>();
    this.hitcallback = _array;
    Array<Procedure0> _array_1 = new Array<Procedure0>();
    this.deadcallback = _array_1;
  }
  
  public Health(final int currredhp, final int currbluehp, final int currwhitehp, final int maxredhp) {
    this(currredhp, currbluehp, currwhitehp, maxredhp, true);
  }
  
  public Health(final int starthp) {
    this(starthp, 0, 0, starthp);
  }
  
  public void pickUpHeart(final HeartType heart) {
    boolean _or = false;
    boolean _equals = Objects.equal(heart, HeartType.redHeart);
    if (_equals) {
      _or = true;
    } else {
      boolean _equals_1 = Objects.equal(heart, HeartType.halfredHeart);
      _or = (_equals || _equals_1);
    }
    if (_or) {
      int redheartdif = (this.maxredhp - this.currredhp);
      if ((redheartdif == 0)) {
        return;
      } else {
        if ((redheartdif == 1)) {
          this.currredhp = (this.currredhp + 1);
        } else {
          if ((redheartdif > 1)) {
            boolean _equals_2 = Objects.equal(heart, HeartType.redHeart);
            if (_equals_2) {
              this.currredhp = (this.currredhp + 2);
            } else {
              this.currredhp = (this.currredhp + 1);
            }
          }
        }
      }
    } else {
      boolean _equals_3 = Objects.equal(heart, HeartType.blueHeart);
      if (_equals_3) {
        this.currbluehp = (this.currbluehp + 1);
      } else {
        boolean _equals_4 = Objects.equal(heart, HeartType.whiteHeart);
        if (_equals_4) {
          if ((this.currwhitehp == 1)) {
            this.addHeartContainer(false);
          } else {
            this.currwhitehp = (this.currwhitehp + 1);
          }
        }
      }
    }
  }
  
  public void addHeartContainer(final boolean filled) {
    if (this.canhaveredheart) {
      this.maxredhp = (this.maxredhp + 1);
      if (filled) {
        this.pickUpHeart(HeartType.redHeart);
      }
    } else {
      this.pickUpHeart(HeartType.blueHeart);
    }
  }
  
  /**
   * if ignoreblue is true it will damage:
   * white->red(down to a half heart)->blue
   */
  private void damage(final int damage, final boolean ignoreblue, final DamageSource source, final boolean force) {
    boolean _isInvincible = this.isInvincible();
    boolean _not = (!_isInvincible);
    if (_not) {
      boolean _or = false;
      final Function1<Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean>,Boolean> _function = new Function1<Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean>,Boolean>() {
        public Boolean apply(final Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean> it) {
          Boolean _apply = it.apply(Integer.valueOf(damage), Boolean.valueOf(ignoreblue), source);
          return _apply;
        }
      };
      boolean _forall = IterableExtensions.<Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean>>forall(this.hitcallback, _function);
      if (_forall) {
        _or = true;
      } else {
        _or = (_forall || force);
      }
      if (_or) {
        int dmg = damage;
        boolean _while = (dmg >= 1);
        while (_while) {
          {
            boolean _or_1 = false;
            if (((!ignoreblue) && (this.currbluehp > 0))) {
              _or_1 = true;
            } else {
              _or_1 = (((!ignoreblue) && (this.currbluehp > 0)) || ((this.currredhp == 1) && (this.currbluehp > 0)));
            }
            if (_or_1) {
              this.currbluehp = (this.currbluehp - 1);
            } else {
              if ((this.currwhitehp > 0)) {
                this.currwhitehp = (this.currwhitehp - 1);
              } else {
                if ((this.currredhp > 0)) {
                  this.currredhp = (this.currredhp - 1);
                }
              }
            }
            int _healthSum = this.getHealthSum();
            boolean _lessEqualsThan = (_healthSum <= 0);
            if (_lessEqualsThan) {
              final Procedure1<Procedure0> _function_1 = new Procedure1<Procedure0>() {
                public void apply(final Procedure0 it) {
                  it.apply();
                }
              };
              IterableExtensions.<Procedure0>forEach(this.deadcallback, _function_1);
              this.dead = true;
            }
            dmg = (dmg - 1);
          }
          _while = (dmg >= 1);
        }
        final Procedure1<Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean>> _function_1 = new Procedure1<Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean>>() {
          public void apply(final Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean> it) {
            it.apply(Integer.valueOf(damage), Boolean.valueOf(ignoreblue), source);
          }
        };
        IterableExtensions.<Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean>>forEach(this.hitcallback, _function_1);
      }
    }
  }
  
  public void damage(final int damage, final DamageSource source) {
    this.damage(damage, false, source, false);
  }
  
  public void addHitcallback(final Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean> callback) {
    this.hitcallback.add(callback);
  }
  
  public void removeHitCallback(final Function3<? super Integer,? super Boolean,? super DamageSource,? extends Boolean> callback) {
    this.hitcallback.removeValue(callback, false);
  }
  
  public void addDeadcallback(final Procedure0 callback) {
    this.deadcallback.add(callback);
  }
  
  public void removeDeadCallback(final Procedure0 callback) {
    this.deadcallback.removeValue(callback, false);
  }
  
  public boolean isDead() {
    return this.dead;
  }
  
  public int getHealthSum() {
    return ((this.currbluehp + this.currredhp) + this.currwhitehp);
  }
  
  public String toString() {
    String _xblockexpression = null;
    {
      super.toString();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("currredhp = ");
      _builder.append(this.currredhp, "");
      _builder.append("/");
      _builder.append(this.maxredhp, "");
      _builder.append(", currwhitehp = ");
      _builder.append(this.currwhitehp, "");
      _builder.append(", currbluehp = ");
      _builder.append(this.currbluehp, "");
      _xblockexpression = (_builder.toString());
    }
    return _xblockexpression;
  }
}
