package markuski.jsaac.entity.health;

@SuppressWarnings("all")
public enum HeartType {
  redHeart,
  
  blueHeart,
  
  whiteHeart,
  
  halfredHeart;
}
