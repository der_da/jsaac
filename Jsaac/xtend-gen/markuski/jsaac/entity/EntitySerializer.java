package markuski.jsaac.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;
import java.lang.reflect.Constructor;
import markuski.jsaac.access.GameAccess;
import markuski.jsaac.entity.Entity;
import markuski.jsaac.entity.EntityDef;
import markuski.jsaac.entity.manager.EntityManager;
import markuski.jsaac.phy.hitbox.Hitbox;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class EntitySerializer {
  public static CharSequence serilize(final Entity obj) {
    CharSequence _xblockexpression = null;
    {
      Hitbox _hitbox = obj.getHitbox();
      Vector2 pos = _hitbox.getPosition();
      EntityDef _entityDef = obj.getEntityDef();
      Class<? extends EntityDef> _class = _entityDef.getClass();
      String str = _class.getName();
      Package _package = Entity.class.getPackage();
      String _name = _package.getName();
      String str2 = (_name + ".entities.");
      String _xifexpression = null;
      boolean _startsWith = str.startsWith(str2);
      if (_startsWith) {
        String _replace = str.replace(str2, "");
        _xifexpression = _replace;
      } else {
        _xifexpression = str;
      }
      String clazz = _xifexpression;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("[\"");
      _builder.append(clazz, "");
      _builder.append("\", ");
      EntityDef _entityDef_1 = obj.getEntityDef();
      Array<String> _data = _entityDef_1.getData(obj);
      String _json = GameAccess.JSON.toJson(_data);
      _builder.append(_json, "");
      _builder.append(", ");
      _builder.append(pos.x, "");
      _builder.append(", ");
      _builder.append(pos.y, "");
      _builder.append("]");
      _builder.newLineIfNotEmpty();
      _xblockexpression = (_builder);
    }
    return _xblockexpression;
  }
  
  public static EntityDef deserilize(final JsonValue jsonData) {
    try {
      EntityDef _xblockexpression = null;
      {
        JsonValue _get = jsonData.get(0);
        String str = _get.asString();
        EntityDef _xtrycatchfinallyexpression = null;
        try {
          Class<? extends Object> _forName = Class.forName(str);
          Constructor<? extends Object> _constructor = _forName.getConstructor();
          Object _newInstance = _constructor.newInstance();
          _xtrycatchfinallyexpression = ((EntityDef) _newInstance);
        } catch (final Throwable _t) {
          if (_t instanceof Exception) {
            final Exception e = (Exception)_t;
            EntityDef _xtrycatchfinallyexpression_1 = null;
            try {
              Package _package = Entity.class.getPackage();
              String _name = _package.getName();
              String _plus = (_name + ".entities.");
              String _plus_1 = (_plus + str);
              Class<? extends Object> _forName_1 = Class.forName(_plus_1);
              Constructor<? extends Object> _constructor_1 = _forName_1.getConstructor();
              Object _newInstance_1 = _constructor_1.newInstance();
              _xtrycatchfinallyexpression_1 = ((EntityDef) _newInstance_1);
            } catch (final Throwable _t_1) {
              if (_t_1 instanceof Exception) {
                final Exception e1 = (Exception)_t_1;
                throw e1;
              } else {
                throw Exceptions.sneakyThrow(_t_1);
              }
            }
            _xtrycatchfinallyexpression = _xtrycatchfinallyexpression_1;
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
        _xblockexpression = (_xtrycatchfinallyexpression);
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static Entity deserilize(final JsonValue jsonData, final EntityManager ew) {
    Entity _xblockexpression = null;
    {
      JsonValue _get = jsonData.get(1);
      String _string = _get.toString();
      Array _fromJson = GameAccess.JSON.<Array>fromJson(Array.class, _string);
      InputOutput.<Array>println(_fromJson);
      EntityDef _deserilize = EntitySerializer.deserilize(jsonData);
      JsonValue _get_1 = jsonData.get(2);
      float _asFloat = _get_1.asFloat();
      JsonValue _get_2 = jsonData.get(3);
      float _asFloat_1 = _get_2.asFloat();
      JsonValue _get_3 = jsonData.get(1);
      String _string_1 = _get_3.toString();
      Array _fromJson_1 = GameAccess.JSON.<Array>fromJson(Array.class, _string_1);
      Entity _createEntity = ew.createEntity(_deserilize, _asFloat, _asFloat_1, _fromJson_1);
      _xblockexpression = (_createEntity);
    }
    return _xblockexpression;
  }
}
