package markuski.jsaac.utils;

import markuski.jsaac.utils.TypeStorer2;

@SuppressWarnings("all")
public class TypeStorer3<TypeA extends Object, TypeB extends Object, TypeC extends Object> extends TypeStorer2<TypeA,TypeB> {
  private TypeC _c;
  
  public TypeC getC() {
    return this._c;
  }
  
  public void setC(final TypeC c) {
    this._c = c;
  }
  
  public TypeStorer3(final TypeA a, final TypeB b, final TypeC c) {
    super(a, b);
    this.setC(c);
  }
  
  public int getSize() {
    return 3;
  }
}
