package markuski.jsaac.utils;

@SuppressWarnings("all")
public class TypeStorer5<TypeA extends Object, TypeB extends Object, TypeC extends Object, TypeD extends Object, TypeE extends Object, TypeF extends Object> {
  private TypeA _a;
  
  public TypeA getA() {
    return this._a;
  }
  
  public void setA(final TypeA a) {
    this._a = a;
  }
  
  private TypeB _b;
  
  public TypeB getB() {
    return this._b;
  }
  
  public void setB(final TypeB b) {
    this._b = b;
  }
  
  private TypeC _c;
  
  public TypeC getC() {
    return this._c;
  }
  
  public void setC(final TypeC c) {
    this._c = c;
  }
  
  private TypeD _d;
  
  public TypeD getD() {
    return this._d;
  }
  
  public void setD(final TypeD d) {
    this._d = d;
  }
  
  private TypeE _e;
  
  public TypeE getE() {
    return this._e;
  }
  
  public void setE(final TypeE e) {
    this._e = e;
  }
  
  private TypeF _f;
  
  public TypeF getF() {
    return this._f;
  }
  
  public void setF(final TypeF f) {
    this._f = f;
  }
  
  public TypeStorer5(final TypeA a, final TypeB b, final TypeC c, final TypeD d, final TypeE e, final TypeF f) {
    this.setA(a);
    this.setB(b);
    this.setC(c);
    this.setD(d);
    this.setE(e);
    this.setF(f);
  }
}
