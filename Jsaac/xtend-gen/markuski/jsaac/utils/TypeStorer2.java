package markuski.jsaac.utils;

import markuski.jsaac.utils.TypeStorer;

@SuppressWarnings("all")
public class TypeStorer2<TypeA extends Object, TypeB extends Object> extends TypeStorer<TypeA> {
  private TypeB _b;
  
  public TypeB getB() {
    return this._b;
  }
  
  public void setB(final TypeB b) {
    this._b = b;
  }
  
  public TypeStorer2(final TypeA a, final TypeB b) {
    super(a);
    this.setB(b);
  }
  
  public int getSize() {
    return 2;
  }
}
