package markuski.jsaac.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.util.Map;
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite;
import net.dermetfan.utils.libgdx.graphics.AnimatedSprite;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class GameUtils {
  public final static int BLOCK = 1;
  
  public final static int PLAYER = 100;
  
  private static Map<String,Sprite> Spritecache = CollectionLiterals.<String, Sprite>newHashMap();
  
  public static void nameAndFPS() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("JSaac FPS:");
    int _framesPerSecond = Gdx.graphics.getFramesPerSecond();
    _builder.append(_framesPerSecond, "");
    Gdx.graphics.setTitle(_builder.toString());
  }
  
  public static Sprite getSprite(final String path, final int w, final int h) {
    Sprite _xifexpression = null;
    boolean _containsKey = GameUtils.Spritecache.containsKey(((((path + "w") + Integer.valueOf(w)) + "h") + Integer.valueOf(h)));
    if (_containsKey) {
      return GameUtils.Spritecache.get(((((path + "w") + Integer.valueOf(w)) + "h") + Integer.valueOf(h)));
    } else {
      Sprite _xblockexpression = null;
      {
        Sprite _xtrycatchfinallyexpression = null;
        try {
          Texture _texture = new Texture(path);
          Sprite _sprite = new Sprite(_texture);
          _xtrycatchfinallyexpression = _sprite;
        } catch (final Throwable _t) {
          if (_t instanceof GdxRuntimeException) {
            final GdxRuntimeException e = (GdxRuntimeException)_t;
            Texture _texture_1 = new Texture("img/imgerror.png");
            Sprite _sprite_1 = new Sprite(_texture_1);
            _xtrycatchfinallyexpression = _sprite_1;
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
        Sprite s = _xtrycatchfinallyexpression;
        s.setSize(w, h);
        GameUtils.Spritecache.put(((((path + "w") + Integer.valueOf(w)) + "h") + Integer.valueOf(h)), s);
        _xblockexpression = (s);
      }
      _xifexpression = _xblockexpression;
    }
    return _xifexpression;
  }
  
  public static AnimatedBox2DSprite getAnimatedBox2DSprite(final float frameDuration, final int zindex, final String... paths) {
    AnimatedBox2DSprite _xblockexpression = null;
    {
      Array<Sprite> _array = new Array<Sprite>();
      Array<Sprite> keyFrames = _array;
      int i = 0;
      int _length = paths.length;
      boolean _lessThan = (i < _length);
      boolean _while = _lessThan;
      while (_while) {
        {
          String _get = paths[i];
          Sprite _sprite = GameUtils.getSprite(_get, 80, 80);
          keyFrames.add(_sprite);
          i = (i + 1);
        }
        int _length_1 = paths.length;
        boolean _lessThan_1 = (i < _length_1);
        _while = _lessThan_1;
      }
      Animation _animation = new Animation(frameDuration, keyFrames);
      AnimatedSprite _animatedSprite = new AnimatedSprite(_animation);
      AnimatedBox2DSprite _animatedBox2DSprite = new AnimatedBox2DSprite(_animatedSprite);
      AnimatedBox2DSprite sprite = _animatedBox2DSprite;
      sprite.setZ(zindex);
      _xblockexpression = (sprite);
    }
    return _xblockexpression;
  }
  
  public static <T extends Object> Array<T> newArray(final T... initail) {
    Array<T> _xblockexpression = null;
    {
      Array<T> _array = new Array<T>();
      Array<T> result = _array;
      result.addAll(initail);
      _xblockexpression = (result);
    }
    return _xblockexpression;
  }
}
