package markuski.jsaac.utils;

@SuppressWarnings("all")
public class TypeStorer<TypeA extends Object> {
  private TypeA _a;
  
  public TypeA getA() {
    return this._a;
  }
  
  public void setA(final TypeA a) {
    this._a = a;
  }
  
  public TypeStorer(final TypeA a) {
    this.setA(a);
  }
  
  public int getSize() {
    return 1;
  }
}
