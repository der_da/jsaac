package markuski.jsaac;

import com.badlogic.gdx.Game;
import markuski.jsaac.access.GameAccess;
import markuski.jsaac.level.DebugLevel;

@SuppressWarnings("all")
public class Jsaac extends Game {
  public void create() {
    DebugLevel _debugLevel = new DebugLevel();
    GameAccess.GAME.setScreen(_debugLevel);
  }
}
