package markuski.jsaac.access;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;
import org.eclipse.xtext.xbase.lib.Functions.Function0;

@SuppressWarnings("all")
public class GameAccess {
  public final static Game GAME = ((Game) Gdx.app.getApplicationListener());
  
  public final static Json JSON = new Function0<Json>() {
    public Json apply() {
      Json _json = new Json();
      return _json;
    }
  }.apply();
}
