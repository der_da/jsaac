package markuski.jsaac.phy.hitbox;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;
import markuski.jsaac.phy.hitbox.HitboxDef;
import markuski.jsaac.phy.hitbox.world.HitboxWorld;
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class Hitbox {
  private Body body;
  
  private Array<Fixture> fix;
  
  private HitboxWorld world;
  
  private HitboxDef h;
  
  private AnimatedBox2DSprite _look;
  
  public Hitbox(final Body b, final HitboxDef h, final HitboxWorld w) {
    this.world = w;
    this.h = h;
    this.body = b;
  }
  
  /**
   * Get the world body origin position.
   * @return the world position of the body's origin.
   */
  public Vector2 getPosition() {
    Vector2 _position = this.body.getPosition();
    return _position;
  }
  
  /**
   * Get the angle in radians.
   * @return the current world rotation angle in radians.
   */
  public float getAngle() {
    float _angle = this.body.getAngle();
    return _angle;
  }
  
  /**
   * Set the linear velocity of the center of mass.
   */
  public void setLinearVelocity(final Vector2 v) {
    this.body.setLinearVelocity(v);
  }
  
  /**
   * Get the linear velocity of the center of mass.
   */
  public Vector2 getLinearVelocity() {
    Vector2 _linearVelocity = this.body.getLinearVelocity();
    return _linearVelocity;
  }
  
  /**
   * Set the angular velocity.
   */
  public void setAngularVelocity(final float omega) {
    this.body.setAngularVelocity(omega);
  }
  
  /**
   * Get the angular velocity.
   */
  public float getAngularVelocity() {
    float _angularVelocity = this.body.getAngularVelocity();
    return _angularVelocity;
  }
  
  /**
   * Apply a force at a world point. If the force is not applied at the center of mass, it will generate a torque and affect the
   * angular velocity. This wakes up the body.
   * @param force the world force vector, usually in Newtons (N).
   * @param point the world position of the point of application.
   * @param wake up the body
   */
  public void applyForce(final Vector2 force, final Vector2 point, final boolean wake) {
    Vector2 force2 = force;
    Vector2 point2 = point;
    this.body.applyForce(force2, point2, wake);
  }
  
  /**
   * Apply a force to the center of mass. This wakes up the body.
   * @param force the world force vector, usually in Newtons (N).
   */
  public void applyForceToCenter(final Vector2 force, final boolean wake) {
    this.body.applyForceToCenter(force, wake);
  }
  
  /**
   * Apply a torque. This affects the angular velocity without affecting the linear velocity of the center of mass. This wakes up
   * the body.
   * @param torque about the z-axis (out of the screen), usually in N-m.
   * @param wake up the body
   */
  public void applyTorque(final float torque, final boolean wake) {
    this.body.applyTorque(torque, wake);
  }
  
  /**
   * Apply an impulse at a point. This immediately modifies the velocity. It also modifies the angular velocity if the point of
   * application is not at the center of mass. This wakes up the body.
   * @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
   * @param point the world position of the point of application.
   * @param wake up the body
   */
  public void applyLinearImpulse(final Vector2 impulse, final Vector2 point, final boolean wake) {
    this.body.applyForce(impulse, point, wake);
  }
  
  /**
   * Apply an angular impulse.
   * @param impulse the angular impulse in units of kg*m*m/s
   */
  public void applyAngularImpulse(final float impulse, final boolean wake) {
    this.body.applyAngularImpulse(impulse, wake);
  }
  
  /**
   * Get the total mass of the body.
   * @return the mass, usually in kilograms (kg).
   */
  public float getMass() {
    float _mass = this.body.getMass();
    return _mass;
  }
  
  /**
   * Get the rotational inertia of the body about the local origin.
   * @return the rotational inertia, usually in kg-m^2.
   */
  public float getInertia() {
    float _inertia = this.body.getInertia();
    return _inertia;
  }
  
  /**
   * Get the world coordinates of a point given the local coordinates.
   * @param localPoint a point on the body measured relative the the body's origin.
   * @return the same point expressed in world coordinates.
   */
  public Vector2 getWorldPoint(final Vector2 localPoint) {
    Vector2 _worldPoint = this.body.getWorldPoint(localPoint);
    return _worldPoint;
  }
  
  /**
   * Get the world coordinates of a vector given the local coordinates.
   * @param localVector a vector fixed in the body.
   * @return the same vector expressed in world coordinates.
   */
  public Vector2 getWorldVector(final Vector2 localVector) {
    Vector2 _worldVector = this.body.getWorldVector(localVector);
    return _worldVector;
  }
  
  /**
   * Gets a local point relative to the body's origin given a world point.
   * @param worldPoint a point in world coordinates.
   * @return the corresponding local point relative to the body's origin.
   */
  public Vector2 getLocalPoint(final Vector2 worldPoint) {
    Vector2 _localPoint = this.body.getLocalPoint(worldPoint);
    return _localPoint;
  }
  
  /**
   * Gets a local vector given a world vector.
   * @param worldVector a vector in world coordinates.
   * @return the corresponding local vector.
   */
  public Vector2 getLocalVector(final Vector2 worldVector) {
    Vector2 _localVector = this.body.getLocalVector(worldVector);
    return _localVector;
  }
  
  /**
   * Get the world linear velocity of a world point attached to this body.
   * @param worldPoint a point in world coordinates.
   * @return the world velocity of a point.
   */
  public Vector2 getLinearVelocityFromWorldPoint(final Vector2 worldPoint) {
    Vector2 _linearVelocityFromWorldPoint = this.body.getLinearVelocityFromWorldPoint(worldPoint);
    return _linearVelocityFromWorldPoint;
  }
  
  /**
   * Get the world velocity of a local point.
   * @param localPoint a point in local coordinates.
   * @return the world velocity of a point.
   */
  public Vector2 getLinearVelocityFromLocalPoint(final Vector2 localPoint) {
    Vector2 _linearVelocityFromLocalPoint = this.body.getLinearVelocityFromLocalPoint(localPoint);
    return _linearVelocityFromLocalPoint;
  }
  
  /**
   * Get the linear damping of the body.
   */
  public float getLinearDamping() {
    float _linearDamping = this.body.getLinearDamping();
    return _linearDamping;
  }
  
  /**
   * Set the linear damping of the body.
   */
  public void setLinearDamping(final float linearDamping) {
    this.body.setLinearDamping(linearDamping);
  }
  
  /**
   * Get the angular damping of the body.
   */
  public float getAngularDamping() {
    float _angularDamping = this.body.getAngularDamping();
    return _angularDamping;
  }
  
  /**
   * Set the angular damping of the body.
   */
  public void setAngularDamping(final float angularDamping) {
    this.body.setAngularDamping(angularDamping);
  }
  
  /**
   * Set the type of this body. This may alter the mass and velocity.
   */
  public void setType(final BodyDef.BodyType type) {
    this.body.setType(type);
  }
  
  /**
   * Get the type of this body.
   */
  public BodyDef.BodyType getType() {
    BodyDef.BodyType _type = this.body.getType();
    return _type;
  }
  
  /**
   * Should this body be treated like a bullet for continuous collision detection?
   */
  public void setBullet(final boolean flag) {
    this.body.setBullet(flag);
  }
  
  /**
   * Is this body treated like a bullet for continuous collision detection?
   */
  public boolean isBullet() {
    boolean _isBullet = this.body.isBullet();
    return _isBullet;
  }
  
  /**
   * You can disable sleeping on this body. If you disable sleeping, the body will never sleep
   */
  public void setSleepingAllowed(final boolean flag) {
    this.body.setSleepingAllowed(flag);
  }
  
  /**
   * Is this body allowed to sleep
   */
  public boolean isSleepingAllowed() {
    boolean _isSleepingAllowed = this.body.isSleepingAllowed();
    return _isSleepingAllowed;
  }
  
  /**
   * Set the sleep state of the body. A sleeping body has very low CPU cost.
   * @param flag set to true to put body to sleep, false to wake it.
   */
  public void setAwake(final boolean flag) {
    this.body.setAwake(flag);
  }
  
  /**
   * Get the sleeping state of this body.
   * @return true if the body is sleeping.
   */
  public boolean isAwake() {
    boolean _isAwake = this.body.isAwake();
    return _isAwake;
  }
  
  /**
   * Set the active state of the body. An inactive body is not simulated and cannot be collided with or woken up. If you pass a
   * flag of true, all fixtures will be added to the broad-phase. If you pass a flag of false, all fixtures will be removed from
   * the broad-phase and all contacts will be destroyed. Fixtures and joints are otherwise unaffected. You may continue to
   * create/destroy fixtures and joints on inactive bodies. Fixtures on an inactive body are implicitly inactive and will not
   * participate in collisions, ray-casts, or queries. Joints connected to an inactive body are implicitly inactive. An inactive
   * body is still owned by a b2World object and remains in the body list.
   */
  public void setActive(final boolean flag) {
    this.body.setActive(flag);
  }
  
  /**
   * Get the active state of the body.
   */
  public boolean isActive() {
    boolean _isActive = this.body.isActive();
    return _isActive;
  }
  
  /**
   * Set this body to have fixed rotation. This causes the mass to be reset.
   */
  public void setFixedRotation(final boolean flag) {
    this.body.setFixedRotation(flag);
  }
  
  /**
   * Does this body have fixed rotation?
   */
  public boolean isFixedRotation() {
    boolean _isFixedRotation = this.body.isFixedRotation();
    return _isFixedRotation;
  }
  
  /**
   * @return Get the gravity scale of the body.
   */
  public float getGravityScale() {
    float _gravityScale = this.body.getGravityScale();
    return _gravityScale;
  }
  
  /**
   * Sets the gravity scale of the body
   */
  public void setGravityScale(final float scale) {
    this.body.setGravityScale(scale);
  }
  
  /**
   * Get the parent world of this body.
   */
  public HitboxWorld getWorld() {
    return this.world;
  }
  
  /**
   * Get the look
   */
  public AnimatedBox2DSprite getLook() {
    Object _userData = this.body.getUserData();
    return ((AnimatedBox2DSprite) _userData);
  }
  
  /**
   * Set the look
   */
  public void setLook(final AnimatedBox2DSprite s) {
    this.body.setUserData(s);
  }
  
  /**
   * Set if this is NonSolid.
   */
  public void setNonSolid(final boolean sensor) {
    final Procedure1<Fixture> _function = new Procedure1<Fixture>() {
      public void apply(final Fixture it) {
        it.setSensor(sensor);
      }
    };
    IterableExtensions.<Fixture>forEach(this.fix, _function);
  }
  
  /**
   * Is this fixture a sensor (non-solid)?
   * @return the true if the shape is a sensor.
   */
  public boolean isNonSolid() {
    Fixture _first = this.fix.first();
    boolean _isSensor = _first.isSensor();
    return _isSensor;
  }
  
  /**
   * Test a point for containment in this fixture.
   * @param p a point in world coordinates.
   */
  public boolean testPoint(final Vector2 p) {
    final Function1<Fixture,Boolean> _function = new Function1<Fixture,Boolean>() {
      public Boolean apply(final Fixture it) {
        boolean _testPoint = it.testPoint(p);
        return Boolean.valueOf(_testPoint);
      }
    };
    boolean _forall = IterableExtensions.<Fixture>forall(this.fix, _function);
    return _forall;
  }
  
  /**
   * Set the density of this fixture.
   */
  public void set_Density(final float density) {
    final Procedure1<Fixture> _function = new Procedure1<Fixture>() {
      public void apply(final Fixture it) {
        it.setDensity(density);
      }
    };
    IterableExtensions.<Fixture>forEach(this.fix, _function);
    this.body.resetMassData();
  }
  
  /**
   * Get the density of this fixture.
   */
  public float get_Density() {
    Fixture _first = this.fix.first();
    float _density = _first.getDensity();
    return _density;
  }
  
  /**
   * Get the coefficient of friction.
   */
  public float get_Friction() {
    Fixture _first = this.fix.first();
    float _friction = _first.getFriction();
    return _friction;
  }
  
  /**
   * Set the coefficient of friction.
   */
  public void set_Friction(final float friction) {
    final Procedure1<Fixture> _function = new Procedure1<Fixture>() {
      public void apply(final Fixture it) {
        it.setFriction(friction);
      }
    };
    IterableExtensions.<Fixture>forEach(this.fix, _function);
  }
  
  /**
   * Get the coefficient of restitution.
   */
  public float get_Restitution() {
    Fixture _first = this.fix.first();
    float _restitution = _first.getRestitution();
    return _restitution;
  }
  
  /**
   * Set the coefficient of restitution.
   */
  public void set_Restitution(final float restitution) {
    final Procedure1<Fixture> _function = new Procedure1<Fixture>() {
      public void apply(final Fixture it) {
        it.setRestitution(restitution);
      }
    };
    IterableExtensions.<Fixture>forEach(this.fix, _function);
  }
}
