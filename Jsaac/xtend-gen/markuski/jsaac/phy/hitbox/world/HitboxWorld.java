package markuski.jsaac.phy.hitbox.world;

import aurelienribon.bodyeditor.BodyEditorLoader;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.google.common.base.Objects;
import markuski.jsaac.phy.hitbox.Hitbox;
import markuski.jsaac.phy.hitbox.HitboxDef;
import markuski.jsaac.utils.TypeStorer2;
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class HitboxWorld implements Disposable {
  private World world;
  
  private Array<Hitbox> hitboxes;
  
  private Array<Body> bodies;
  
  private Box2DDebugRenderer debug;
  
  public HitboxWorld() {
    Vector2 _vector2 = new Vector2();
    World _world = new World(_vector2, true);
    this.world = _world;
    Array<Hitbox> _array = new Array<Hitbox>();
    this.hitboxes = _array;
    Array<Body> _array_1 = new Array<Body>();
    this.bodies = _array_1;
  }
  
  public Hitbox createHitbox(final HitboxDef d) {
    Hitbox _hitbox = d.toHitbox(this);
    return ((Hitbox) _hitbox);
  }
  
  public void destoryHitbox(final Hitbox h) {
    int i = this.hitboxes.indexOf(h, false);
    Body _get = this.bodies.get(i);
    this.world.destroyBody(_get);
    this.hitboxes.removeIndex(i);
    this.hitboxes.shrink();
    this.bodies.shrink();
  }
  
  @Deprecated
  public Hitbox createHitbox(final BodyDef b, final FixtureDef f, final HitboxDef h) {
    Hitbox _xblockexpression = null;
    {
      Body body = this.world.createBody(b);
      AnimatedBox2DSprite _look = h.getLook();
      body.setUserData(_look);
      BodyEditorLoader _loader = h.getLoader();
      String _bodyName = h.getBodyName();
      float _scale = h.getScale();
      _loader.attachFixture(body, _bodyName, f, _scale);
      Hitbox _hitbox = new Hitbox(body, h, this);
      Hitbox hitbox = _hitbox;
      this.hitboxes.add(hitbox);
      this.bodies.add(body);
      _xblockexpression = (hitbox);
    }
    return _xblockexpression;
  }
  
  public Array<TypeStorer2<Hitbox,Hitbox>> getContacts() {
    Array<TypeStorer2<Hitbox,Hitbox>> _xblockexpression = null;
    {
      Array<TypeStorer2<Hitbox,Hitbox>> _array = new Array<TypeStorer2<Hitbox, Hitbox>>();
      final Array<TypeStorer2<Hitbox,Hitbox>> result = _array;
      Array<Contact> _contactList = this.world.getContactList();
      final Procedure1<Contact> _function = new Procedure1<Contact>() {
        public void apply(final Contact it) {
          boolean _isTouching = it.isTouching();
          if (_isTouching) {
            Fixture _fixtureA = it.getFixtureA();
            Body _body = _fixtureA.getBody();
            int _indexOf = HitboxWorld.this.bodies.indexOf(_body, false);
            Hitbox hitboxa = HitboxWorld.this.hitboxes.get(_indexOf);
            Fixture _fixtureB = it.getFixtureB();
            Body _body_1 = _fixtureB.getBody();
            int _indexOf_1 = HitboxWorld.this.bodies.indexOf(_body_1, false);
            Hitbox hitboxb = HitboxWorld.this.hitboxes.get(_indexOf_1);
            TypeStorer2<Hitbox,Hitbox> _typeStorer2 = new TypeStorer2<Hitbox, Hitbox>(hitboxa, hitboxb);
            result.add(_typeStorer2);
          }
        }
      };
      IterableExtensions.<Contact>forEach(_contactList, _function);
      _xblockexpression = (result);
    }
    return _xblockexpression;
  }
  
  public void step(final float timestep) {
    this.world.step(timestep, 3, 8);
  }
  
  public void draw(final SpriteBatch b) {
    AnimatedBox2DSprite.draw(b, this.world, true);
  }
  
  public void drawDebug(final SpriteBatch b) {
    boolean _equals = Objects.equal(this.debug, null);
    if (_equals) {
      Box2DDebugRenderer _box2DDebugRenderer = new Box2DDebugRenderer(true, true, true, true, true, true);
      this.debug = _box2DDebugRenderer;
    }
    Matrix4 _projectionMatrix = b.getProjectionMatrix();
    this.debug.render(this.world, _projectionMatrix);
  }
  
  public void dispose() {
    this.world.dispose();
  }
}
