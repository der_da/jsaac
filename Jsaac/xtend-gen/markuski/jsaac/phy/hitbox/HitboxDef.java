package markuski.jsaac.phy.hitbox;

import aurelienribon.bodyeditor.BodyEditorLoader;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import markuski.jsaac.phy.hitbox.Hitbox;
import markuski.jsaac.phy.hitbox.world.HitboxWorld;
import markuski.jsaac.utils.GameUtils;
import net.dermetfan.utils.libgdx.graphics.AnimatedBox2DSprite;

@SuppressWarnings("all")
public class HitboxDef {
  private BodyDef body;
  
  private FixtureDef fixture;
  
  private BodyEditorLoader _loader;
  
  public BodyEditorLoader getLoader() {
    return this._loader;
  }
  
  public void setLoader(final BodyEditorLoader loader) {
    this._loader = loader;
  }
  
  private String _bodyName;
  
  public String getBodyName() {
    return this._bodyName;
  }
  
  public void setBodyName(final String bodyName) {
    this._bodyName = bodyName;
  }
  
  private AnimatedBox2DSprite _look;
  
  public AnimatedBox2DSprite getLook() {
    return this._look;
  }
  
  public void setLook(final AnimatedBox2DSprite look) {
    this._look = look;
  }
  
  private float _scale = 1;
  
  public float getScale() {
    return this._scale;
  }
  
  public void setScale(final float scale) {
    this._scale = scale;
  }
  
  public HitboxDef(final BodyEditorLoader loader, final String bodyName) {
    BodyDef _bodyDef = new BodyDef();
    this.body = _bodyDef;
    FixtureDef _fixtureDef = new FixtureDef();
    this.fixture = _fixtureDef;
    this.setLoader(loader);
    this.setBodyName(bodyName);
    String _imagePath = loader.getImagePath(bodyName);
    AnimatedBox2DSprite _animatedBox2DSprite = GameUtils.getAnimatedBox2DSprite(1, 0, _imagePath);
    this.setLook(_animatedBox2DSprite);
  }
  
  /**
   * The body type: static, kinematic, or dynamic. Note: if a dynamic body would have zero mass, the mass is set to one.
   */
  public BodyDef.BodyType setType(final BodyDef.BodyType type) {
    BodyDef.BodyType _type = this.body.type = type;
    return _type;
  }
  
  /**
   * The world position of the body. Avoid creating bodies at the origin since this can lead to many overlapping shapes.
   */
  public Vector2 setPosition(final Vector2 pos) {
    Vector2 _set = this.body.position.set(pos);
    return _set;
  }
  
  /**
   * The world angle of the body in radians.
   */
  public float setAngle(final float angle) {
    float _angle = this.body.angle = angle;
    return _angle;
  }
  
  /**
   * The linear velocity of the body's origin in world co-ordinates.
   */
  public Vector2 setLinearVelocity(final Vector2 v) {
    Vector2 _set = this.body.linearVelocity.set(v);
    return _set;
  }
  
  /**
   * The angular velocity of the body.
   */
  public float setAngularVelocity(final float f) {
    float _angularVelocity = this.body.angularVelocity = f;
    return _angularVelocity;
  }
  
  /**
   * Linear damping is use to reduce the linear velocity. The damping parameter can be larger than 1.0f but the damping effect
   * becomes sensitive to the time step when the damping parameter is large.
   */
  public float setLinearDamping(final float f) {
    float _angularDamping = this.body.angularDamping = f;
    return _angularDamping;
  }
  
  /**
   * Angular damping is use to reduce the angular velocity. The damping parameter can be larger than 1.0f but the damping effect
   * becomes sensitive to the time step when the damping parameter is large.
   */
  public float setAngularDamping(final float f) {
    float _angularDamping = this.body.angularDamping = f;
    return _angularDamping;
  }
  
  /**
   * Set this flag to false if this body should never fall asleep. Note that this increases CPU usage.
   */
  public boolean setAlowSleep(final boolean b) {
    boolean _allowSleep = this.body.allowSleep = b;
    return _allowSleep;
  }
  
  /**
   * Is this body initially awake or sleeping?
   */
  public boolean setAwake(final boolean b) {
    boolean _awake = this.body.awake = b;
    return _awake;
  }
  
  /**
   * The world position of the body. Avoid creating bodies at the origin since this can lead to many overlapping shapes.
   */
  public boolean setFixedRotation(final boolean b) {
    boolean _fixedRotation = this.body.fixedRotation = b;
    return _fixedRotation;
  }
  
  /**
   * Is this a fast moving body that should be prevented from tunneling through other moving bodies? Note that all bodies are
   * prevented from tunneling through kinematic and static bodies. This setting is only considered on dynamic bodies.
   * @warning You should use this flag sparingly since it increases processing time.
   */
  public boolean setBullet(final boolean b) {
    boolean _bullet = this.body.bullet = b;
    return _bullet;
  }
  
  /**
   * Does this body start out active?
   */
  public boolean setActive(final boolean b) {
    boolean _active = this.body.active = b;
    return _active;
  }
  
  /**
   * Scale the gravity applied to this body.
   */
  public float setGravityScale(final float f) {
    float _gravityScale = this.body.gravityScale = f;
    return _gravityScale;
  }
  
  /**
   * The density, usually in kg/m^2.
   */
  public float setDensity(final float f) {
    float _density = this.fixture.density = f;
    return _density;
  }
  
  /**
   * The friction coefficient, usually in the range [0,1].
   */
  public float setFriction(final float f) {
    float _friction = this.fixture.friction = f;
    return _friction;
  }
  
  /**
   * The restitution (elasticity) usually in the range [0,1].
   */
  public float setRestitution(final float f) {
    float _restitution = this.fixture.restitution = f;
    return _restitution;
  }
  
  /**
   * A sensor shape collects contact information but never generates a collision response.
   */
  public boolean setSensor(final boolean b) {
    boolean _isSensor = this.fixture.isSensor = b;
    return _isSensor;
  }
  
  /**
   * The body type: static, kinematic, or dynamic. Note: if a dynamic body would have zero mass, the mass is set to one.
   */
  public BodyDef.BodyType getType() {
    return this.body.type;
  }
  
  public Vector2 getPosition() {
    return this.body.position;
  }
  
  /**
   * The world angle of the body in radians.
   */
  public float getAngle() {
    return this.body.angle;
  }
  
  /**
   * The linear velocity of the body's origin in world co-ordinates.
   */
  public Vector2 getLinearVelocity() {
    return this.body.linearVelocity;
  }
  
  /**
   * The angular velocity of the body.
   */
  public float getAngularVelocity() {
    return this.body.angularVelocity;
  }
  
  /**
   * Linear damping is use to reduce the linear velocity. The damping parameter can be larger than 1.0f but the damping effect
   * becomes sensitive to the time step when the damping parameter is large.
   */
  public float getLinearDamping() {
    return this.body.angularDamping;
  }
  
  /**
   * Angular damping is use to reduce the angular velocity. The damping parameter can be larger than 1.0f but the damping effect
   * becomes sensitive to the time step when the damping parameter is large.
   */
  public float getAngularDamping() {
    return this.body.angularDamping;
  }
  
  /**
   * Set this flag to false if this body should never fall asleep. Note that this increases CPU usage.
   */
  public boolean getAlowSleep() {
    return this.body.allowSleep;
  }
  
  /**
   * Is this body initially awake or sleeping?
   */
  public boolean getAwake() {
    return this.body.awake;
  }
  
  public boolean getFixedRotation() {
    return this.body.fixedRotation;
  }
  
  /**
   * Is this a fast moving body that should be prevented from tunneling through other moving bodies? Note that all bodies are
   * prevented from tunneling through kinematic and static bodies. This setting is only considered on dynamic bodies.
   * @warning You should use this flag sparingly since it increases processing time.
   */
  public boolean getBullet() {
    return this.body.bullet;
  }
  
  /**
   * Does this body start out active?
   */
  public boolean getActive() {
    return this.body.active;
  }
  
  /**
   * Scale the gravity applied to this body.
   */
  public float getGravityScale() {
    return this.body.gravityScale;
  }
  
  /**
   * The density, usually in kg/m^2.
   */
  public float getDensity() {
    return this.fixture.density;
  }
  
  /**
   * The friction coefficient, usually in the range [0,1].
   */
  public float getFriction() {
    return this.fixture.friction;
  }
  
  /**
   * The restitution (elasticity) usually in the range [0,1].
   */
  public float getRestitution() {
    return this.fixture.restitution;
  }
  
  /**
   * A sensor shape collects contact information but never generates a collision response.
   */
  public boolean getSensor() {
    return this.fixture.isSensor;
  }
  
  @Deprecated
  public Hitbox toHitbox(final HitboxWorld w) {
    Hitbox _createHitbox = w.createHitbox(this.body, this.fixture, this);
    return _createHitbox;
  }
  
  public HitboxDef copy() {
    HitboxDef _xblockexpression = null;
    {
      BodyEditorLoader _loader = this.getLoader();
      String _bodyName = this.getBodyName();
      HitboxDef _hitboxDef = new HitboxDef(_loader, _bodyName);
      HitboxDef h = _hitboxDef;
      boolean _active = this.getActive();
      h.setActive(_active);
      boolean _alowSleep = this.getAlowSleep();
      h.setAlowSleep(_alowSleep);
      float _angle = this.getAngle();
      h.setAngle(_angle);
      float _angularDamping = this.getAngularDamping();
      h.setAngularDamping(_angularDamping);
      float _angularVelocity = this.getAngularVelocity();
      h.setAngularVelocity(_angularVelocity);
      boolean _awake = this.getAwake();
      h.setAwake(_awake);
      boolean _bullet = this.getBullet();
      h.setBullet(_bullet);
      float _density = this.getDensity();
      h.setDensity(_density);
      boolean _fixedRotation = this.getFixedRotation();
      h.setFixedRotation(_fixedRotation);
      float _gravityScale = this.getGravityScale();
      h.setGravityScale(_gravityScale);
      float _linearDamping = this.getLinearDamping();
      h.setLinearDamping(_linearDamping);
      Vector2 _linearVelocity = this.getLinearVelocity();
      h.setLinearVelocity(_linearVelocity);
      BodyEditorLoader _loader_1 = this.getLoader();
      h.setLoader(_loader_1);
      AnimatedBox2DSprite _look = this.getLook();
      h.setLook(_look);
      Vector2 _position = this.getPosition();
      h.setPosition(_position);
      float _restitution = this.getRestitution();
      h.setRestitution(_restitution);
      boolean _sensor = this.getSensor();
      h.setSensor(_sensor);
      _xblockexpression = (h);
    }
    return _xblockexpression;
  }
}
