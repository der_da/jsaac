package markuski.jsaac.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL11;
import markuski.jsaac.entity.entities.Player;
import markuski.jsaac.entity.manager.EntityManager;
import markuski.jsaac.map.RoomGenerator;

@SuppressWarnings("all")
public class DebugLevel extends InputAdapter implements Screen {
  private EntityManager em;
  
  public void dispose() {
    this.hide();
  }
  
  public void hide() {
  }
  
  public void pause() {
  }
  
  public void render(final float delta) {
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT);
    this.em.update(delta);
  }
  
  public void resize(final int width, final int height) {
  }
  
  public void resume() {
  }
  
  public void show() {
    EntityManager _entityManager = new EntityManager(1, 2, 80);
    this.em = _entityManager;
    Player _player = new Player();
    this.em.createEntity(_player, 1, 0);
    FileHandle _internal = Gdx.files.internal("room/testRoom.jroom");
    new RoomGenerator(_internal, this.em, 0, 0);
  }
}
