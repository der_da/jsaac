package markuski.jsaac.map;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.google.common.base.Objects;
import markuski.jsaac.entity.EntitySerializer;
import markuski.jsaac.entity.manager.EntityManager;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class RoomGenerator {
  private int type;
  
  private int height;
  
  private int width;
  
  public RoomGenerator(final FileHandle roomFile, final EntityManager w, final int x, final int y) {
    String _extension = roomFile.extension();
    String _lowerCase = _extension.toLowerCase();
    boolean _notEquals = (!Objects.equal(_lowerCase, "jroom"));
    if (_notEquals) {
      RuntimeException _runtimeException = new RuntimeException("not a correct Jsaac Room");
      throw _runtimeException;
    }
    JsonReader _jsonReader = new JsonReader();
    JsonValue room = _jsonReader.parse(roomFile);
    JsonValue _get = room.get("type");
    int _asInt = _get.asInt();
    this.type = _asInt;
    JsonValue _get_1 = room.get("size");
    JsonValue _get_2 = _get_1.get("h");
    int _asInt_1 = _get_2.asInt();
    this.height = _asInt_1;
    JsonValue _get_3 = room.get("size");
    JsonValue _get_4 = _get_3.get("w");
    int _asInt_2 = _get_4.asInt();
    this.width = _asInt_2;
    JsonValue _get_5 = room.get("entities");
    final Procedure1<JsonValue> _function = new Procedure1<JsonValue>() {
      public void apply(final JsonValue it) {
        String _plus = (it + "   ");
        String _plus_1 = (_plus + w);
        InputOutput.<String>println(_plus_1);
        EntitySerializer.deserilize(it, w);
      }
    };
    IterableExtensions.<JsonValue>forEach(_get_5, _function);
  }
}
