package markuski.jsaac.map;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

@SuppressWarnings("all")
class MapGenerator {
  private final JsonValue floorInfo;
  
  private final Array<JsonValue> rooms;
  
  public MapGenerator(final FileHandle mapHome) {
    JsonReader _jsonReader = new JsonReader();
    JsonReader json = _jsonReader;
    FileHandle _child = mapHome.child("map.json");
    JsonValue _parse = json.parse(_child);
    this.floorInfo = _parse;
    Array<JsonValue> _array = new Array<JsonValue>();
    this.rooms = _array;
  }
}
